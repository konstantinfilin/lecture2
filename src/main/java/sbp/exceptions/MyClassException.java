package sbp.exceptions;

/**
 * класс обрабатывает исключение
 *
 * Наследуется от класса Exception, передает полученное исключение в super класс
 * выводит описание exception message и stacktrace в консоль
 */
public class MyClassException extends Exception
{
    MyClassException(String message, Throwable exception)
    {
        super( message  + " my class Exception test",  exception);
    }
}