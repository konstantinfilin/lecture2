package sbp.exceptions;

public class WorkWithExceptions
{
    /**
     * В данном методе необходимо вызвать методы throwCheckedException и throwUncheckedException.
     * Все исключения должны быть обработаны
     * Необходимо вывести описание exception и stacktrace в консоль
     * Впойманное исключение необходимо упаковать в кастомное исключение и пробросить выше
     * Перед завершением работы метода обязательно необходимо вывести в консоль сообщение "Finish"
     */
    public void exceptionProcessing() throws MyClassException {
        try
        {
            throwUncheckedException();
            throwCheckedException();

        }
        catch ( Exception  e)
        {
            System.out.println("Exception in exceptionProcessing " + e.getMessage());
            e.printStackTrace();
            throw new MyClassException("Exception in exceptionProcessing : ",e);
        }
        finally
        {
            System.out.println("Finish");
        }
    }

    /**
     * (* - необязательно) Доп. задание.
     * Выписать в javadoc (здесь) - все варианты оптимизации и устранения недочётов метода
     * @throws IllegalStateException
     * @throws Exception
     * @throws RuntimeException
     * 1. В блоке finally созадется новое исключение, при этом теряется информация о первоначальном
     * исключении.
     * 2. В блоке catch RuntimeException e так же еряется информация о первоначальном
     * исключении.
     *
     *Для устранения - надо использовать метод e1.addSuppressed(e)
     */
    public void hardExceptionProcessing() throws IllegalStateException, Exception, RuntimeException
    {
        System.out.println("Start");
        try
        {
            System.out.println("Step 1");
            throw new IllegalArgumentException();
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("Catch IllegalArgumentException");
            throw new RuntimeException("Step 2");
        }
        catch (RuntimeException e)
        {
            System.out.println("Catch RuntimeException");

            throw new RuntimeException("Step 3");
        }
        finally
        {
            System.out.println("Step finally");
            throw new RuntimeException("From finally");
        }
    }

    private void throwCheckedException() throws Exception
    {
        throw new Exception("Checked exception");
    }

    private void throwUncheckedException()
    {
        throw new RuntimeException("Unchecked exception");
    }
}