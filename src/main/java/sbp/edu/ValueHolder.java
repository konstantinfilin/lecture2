package sbp.edu;

public class ValueHolder<T extends Comparable<T>> {

    private T value;

    public ValueHolder(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ValueHolder{" +
                "value=" + value +
                '}';
    }

    public int compare(T other){
        return value.compareTo(other);
    }
}
