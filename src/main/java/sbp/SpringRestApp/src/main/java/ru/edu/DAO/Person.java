package ru.edu.DAO;

import java.util.Comparator;

/**
 * Класс Person - шаблон для создания сущностей Person
 * со свойствами  name, city, age и методом сравнения сущностей Person
 * по мвойству city
 *
 * @author Константин Филин
 * @version 1.0
 */

public class Person implements Comparable<Person> {
    /** поле имя */
    private String name;
    /** поле город */
    private String city;
    /** возраст */
    private int age;

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Person(){};

    /**
     * Конструктор - создание нового объекта Person с определенными значениями
     * @param name - имя
     * @param city - город
     * @param age - возраст
     * @throws IllegalArgumentException - бросает исключение при city == null или name == null
     */
    public Person(String name, String city, int age) {
        if(city == null || name == null ){
            throw new IllegalArgumentException("Person constructor: argument's is null");
        }
        this.name = name.toUpperCase();
        this.city = city.toUpperCase();;
        this.age = age;
    }

    /**
     * Функция получения значения поля {@link Person#name}
     * @return возвращает имя человека
     */
    public String getName() {
        return name;
    }

    /**
     * Функция получения значения поля {@link Person#city}
     * @return возвращает название города
     */
    public String getCity() {
        return city;
    }

    /**
     * Функция получения значения поля {@link Person#age}
     * @return возвращает возраст человека
     */
    public int getAge() {
        return age;
    }

    /**
     * Функция получения стокового представление Person
     */
    @Override
    public String toString(){
        return "Person(" +
                "city:" + this.city +
                ",name:" + this.name +
                ",age:" + this.age + ")";
    }

    /**
     * Функция сравнивает два объекта Person : this и o
     * @param o
     * @return true, если объекты Person равны по всем полям, false в противоположном случае
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return this.name.equals(person.name)
                && this.city.equals(person.city)
                && this.age == person.age;
    }



    /**
     * Функция сортирует объекты Person сначала по названию города, а для значений с одинаковым городом
     * отсортирован по имени в лексикографическом порядке
     * @return возвращает целое число :
     * <0 - this меньше объекта person
     * 0 -  this и объект person совпадают
     * >0 - this больше объекта person
     * @throws IllegalArgumentException - бросает исключение при входном параметре равным null
     */
    @Override
    public int compareTo(Person person) {
        if(person == null){
            throw new IllegalArgumentException("Person compareTo : argument is null");
        }
        return Comparator.comparing(Person::getCity)
                .thenComparing(Person::getName)
                .compare(this,person);
    }

}