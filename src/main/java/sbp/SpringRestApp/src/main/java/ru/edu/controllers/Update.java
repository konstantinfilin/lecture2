package ru.edu.controllers;

/**
 * Класс служит для реализации передачи JSON сообщения со стороны клиента
 * в POST методе update
 */
class Update {
    private String city;
    private String id;
    public Update(){};

    public Update(String city, String id){
        this.city = city;
        this.id = id;
    };

    public String getCity() {
        return city;
    }

    public String getId() {
        return id;
    }
}