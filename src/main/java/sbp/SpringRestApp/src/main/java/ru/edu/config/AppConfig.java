package ru.edu.config;


import org.springframework.context.annotation.*;
import ru.edu.DAO.DBServiceThread;
import ru.edu.DAO.PersonDAOThread;


@Configuration
@ComponentScan("ru.edu.controllers")
@PropertySource("db.properties")
public class AppConfig
{

    @Bean
    public DBServiceThread dbService()
    {
        return new DBServiceThread();
    }

    @Bean
    public PersonDAOThread personDao()
    {
        return new PersonDAOThread(dbService());
    }

}
