package ru.edu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ru.edu.DAO.PersonDAOThread;
import ru.edu.DAO.Person;

import java.util.List;


/**
 * Класс содержит методы для обработки запросов пользователя к Rest серверу
 *
 */
@RestController
@RequestMapping("restapi")
public class AppRestController
{
    @Autowired
    private PersonDAOThread personDAO;

    /**
     * Метод возвращает приветствие по GET запросу пользователя "/restapi/"
     */
    @GetMapping("/")
    public ResponseEntity<String> welcome() {
        return new ResponseEntity<>("Welcome to RestTemplate Example", HttpStatus.OK);
    }


    /**
     * Метод возвращает список всех person из БД по GET запросу "/restapi/persons"
     *
     */
    @GetMapping("/persons")
    public ResponseEntity<List<Person>>getAllPersons() {
           List<Person> list = personDAO.getAllPersons();
           if(list == null || list.isEmpty()){
               return new ResponseEntity<>(HttpStatus.NOT_FOUND);
           }
        return new ResponseEntity<>(list, HttpStatus.OK) ;
    }

    /**
     * Метод возвращает список всех person из БД с именем personName по GET запросу "/restapi/persons/{personName}"
     * на пример: "/restapi/persons/ANNA"
     */
    @GetMapping("/persons/{personName}")
    public ResponseEntity<List<Person>> getPersonByName(@PathVariable("personName")String personName) {
        List<Person> list = personDAO.getPersonsByName(personName);
        if(list == null || list.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(list, HttpStatus.OK) ;
    }

    /**
     * Метод добавляет person в БД, получаемого в виде Person объекта
     * через POST запрос "/restapi/addperson"
     */
    @PostMapping("/addperson")
    public ResponseEntity<Person> addPerson(@RequestBody Person person) {
        if(personDAO.addPerson(person)) {
            return new ResponseEntity<>(person, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    /**
     * Метод удаляет из БД persons с заданным возрастом по GET запросу "/restapi/delpersons/{age}"
     * на пример: "/restapi/delpersons/30"
     *
     */
    @GetMapping("/delpersons/{age}")
    public ResponseEntity<Integer> deletePersons(@PathVariable("age") String age) {
        if(personDAO.deletePersonsByAge(Integer.parseInt(age)) > 0 ) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Метод изменяет город у person в БД по указанному ID, получаемого в виде Update объекта
     * через POST запрос "/restapi/update"
     */
    @PostMapping("/update")
    public ResponseEntity<Integer> updatePerson(@RequestBody Update update) {
        if(personDAO.updateCityPerson(update.getCity(), Integer.parseInt(update.getId())) > 0 ) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
