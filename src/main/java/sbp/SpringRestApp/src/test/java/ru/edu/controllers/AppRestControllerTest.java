package ru.edu.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.edu.DAO.Person;
import ru.edu.DAO.PersonDAOThread;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AppRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    /**
     * Метод возвращает статус ответа сервера на запрос клиента
     * Верный запрос отправлен , получен статус HttpStatus.OK
     */
    @Test
    public void getAllPersons_Success_Test() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/restapi/persons")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Метод возвращает статус ответа сервера на запрос клиента
     * Ошибочный запрос отправлен, получен статус HttpStatus.NOT_FOUND
     */
    @Test
    public void getAllPersons_Fail_Test() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/restapi/pers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    /**
     * Метод возвращает статус ответа сервера на запрос клиента о добавлении person
     * Верный запрос отправлен, получен статус HttpStatus.OK
     */
    @Test
    public void postAddlPersons_Success_Test() throws Exception {
        Person person = new Person("Denis", "Mez", 24);
        PersonDAOThread personDAO = mock(PersonDAOThread.class);
        Mockito.when(personDAO.addPerson(person)).thenReturn(true);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/restapi/addperson")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(person));

        mockMvc.perform(mockRequest)
                .andExpect(status().isOk());
    }

    /**
     * Метод возвращает статус ответа сервера на запрос клиента о добавлении person
     * Ошибочный запрос отправлен, получен статус HttpStatus.NOT_FOUND
     */
    @Test
    public void postAddlPersons_Fail_Test() throws Exception {
        Person person = new Person("Denis", "Mez", 24);
        PersonDAOThread mockPersonDAO = mock(PersonDAOThread.class);
        Mockito.when(mockPersonDAO.addPerson(person)).thenReturn(true);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/restapi/add")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(person));

        mockMvc.perform(mockRequest)
                .andExpect(status().isNotFound());
    }
}
