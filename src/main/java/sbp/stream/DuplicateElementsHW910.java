package sbp.stream;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DuplicateElementsHW910 {

    /**
     * Метод находит дублирующиеся элементы в input, используя TreeSet
     * и возвращает список дублирующихся элементов.
     * @param input
     * @param <T>
     */
    public <T> List<T> duplicateSet(Collection<T> input){
        Set<T> items = new TreeSet<>();
        Set<T> result = new TreeSet<>();

        for(T item : input){
            if( !items.add( item ) ){
                result.add( item );
            }
        }
        return new ArrayList<>(result);
    }

    /**
     * Метод находит дублирующиеся элементы в input, используя TreeSet и Stream Api
     * и возвращает список дублирующихся элементов.
     * @param input
     * @param <T>
     */
    public <T> List<T> duplicateSetStream(Collection<T> input){
        List<T> result = new ArrayList<>();
        Set<T> items = new TreeSet<>();

        input.stream().filter(n -> !items.add(n))
                .collect(Collectors.toSet())
                .forEach(result::add);
        return result;
    }

    /**
     * Метод находит дублирующиеся элементы в input, используя TreeMap
     * и возвращает список дублирующихся элементов.
     * @param input
     * @param <T>
     */
    public <T> List<T> duplicateMap(Collection<T> input){
        List<T> result = new ArrayList<>();
        Map<T,Integer> map = new TreeMap<>();
        for(T key : input){
            map.compute(key, (k, v) -> (v == null) ? 1 : v + 1 );
        }
        for (Map.Entry<T, Integer> tIntegerEntry: map.entrySet()){
            if( tIntegerEntry.getValue() > 1 && !result.contains(tIntegerEntry.getKey()) ) {
                result.add(tIntegerEntry.getKey());
            }
        }
        return result;
    }

    /**
     * Метод находит дублирующиеся элементы в input, используя Stream Api
     * и возвращает список дублирующихся элементов.
     * @param input
     * @param <T>
     */
    public <T> List<T> duplicateMapStream(Collection<T> input) {
        List<T> result = new ArrayList<>();
        input.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .filter(element -> element.getValue() > 1)
                .map(Map.Entry::getKey)
                .forEach(result::add);
        return result;
    }

    /**
     * Метод находит дублирующиеся элементы в input, используя TreeSet и кол-во дублирующихся элементов в входном списке
     * и возвращает список дублирующихся элементов.
     * @param input
     * @param <T>
     */
    public <T> List<T> duplicateSetManual(Collection<T> input){
        List<T> result = new ArrayList<>();
        Set<T> items = new TreeSet<>(input);
        for ( T item: items ) {
            int numberItem = 0;
            for(T curItem: input){
                if(item.equals(curItem)){
                    ++numberItem;
                }
            }
            if(numberItem > 1) {
                if(!result.contains(item)) {
                    result.add(item);
                }
            }
        }
        return result;
    }


}
