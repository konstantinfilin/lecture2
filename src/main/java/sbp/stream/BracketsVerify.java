package sbp.stream;

import java.util.ArrayDeque;
import java.util.Deque;

public class BracketsVerify {

    public BracketsVerify() {};

    public boolean bracketsVerification(String inputString) {
        if(inputString == null || inputString.length() == 0) return false;

        final char roundBracketOpen = '(';
        final char roundBracketClose = ')';

        final char curlyBraceOpen = '{';
        final char curlyBraceClose = '}';

        final char squareBracketOpen = '[';
        final char squareBracketClose = ']';

        final String bracketsOpen = "({[";
        final String bracketsClose = ")}]";
        final String allBrackets = bracketsOpen.concat(bracketsClose);

        Deque<Character> stackBrackets = new ArrayDeque<>();

        int currentInputIndex = 0;
        Character curChar, lastChar;

        do {
            curChar = inputString.charAt(currentInputIndex);

            if( allBrackets.indexOf(curChar) == -1 ) {
                return false;
            }

            if(bracketsOpen.indexOf(curChar) >= 0) {
                stackBrackets.offerLast(curChar);
            }else {
                if ( (lastChar = stackBrackets.pollLast())  == null ){
                    return false;
                }

                switch (curChar){
                    case roundBracketClose:
                        if(lastChar != roundBracketOpen){
                            return false;
                        }
                        break;

                    case curlyBraceClose:
                        if(lastChar != curlyBraceOpen){
                            return false;
                        }
                        break;

                    case squareBracketClose:
                        if(lastChar != squareBracketOpen){
                            return false;
                        }
                        break;
                }
            }
            ++currentInputIndex;

        } while ( currentInputIndex < inputString.length()  );

        return stackBrackets.size() == 0;
    }
}
