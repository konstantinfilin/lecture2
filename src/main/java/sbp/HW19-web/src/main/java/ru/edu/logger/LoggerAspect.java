package ru.edu.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;


/**
 * Класс внедрить логирование для всех методов
 * класса AccountUserController
 */
@Aspect
@Component
public class LoggerAspect {

    @Pointcut("execution(* ru.edu.controller.AccountUserController.*(..))")
    public void userControllerPointcut(){}

    /**
     * Метод beforeMethod реализует логирование перед вызовом методов
     * @param joinPoint
     */
    @Before("userControllerPointcut()")
    public void beforeMethod(JoinPoint joinPoint){
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.println("Method " + methodName + " started args" + args + " started");
    }

    /**
     * Метод afterMethod реализует логирование после вызова методов
     * @param joinPoint
     */
    @AfterReturning("userControllerPointcut()")
    public void afterMethod(JoinPoint joinPoint){
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.println("Method " + methodName + " args" + args + " finished");
    }

    /**
     * Метод afterException реализует логирование в случае бросания методом исключения
     * @param joinPoint, ex
     */
    @AfterThrowing(value = "userControllerPointcut()", throwing = "ex")
    public void afterException(JoinPoint joinPoint, Throwable ex){
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.println("Error: fail to method -" + methodName +
                " args - " + args + " exeption : " + ex.toString());
    }


    private List<String> getArgs(JoinPoint joinPoint){
        List<String> args = new ArrayList<>();//Arrays.asList(joinPoint.getArgs());
        for(int i = 0 ; i < joinPoint.getArgs().length; ++i){
            Object argValue = joinPoint.getArgs()[i];
            args.add("arg." + i + "=" + argValue);
        }
        return args;
    }


}
