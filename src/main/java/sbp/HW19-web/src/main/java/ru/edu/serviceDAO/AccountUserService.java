package ru.edu.serviceDAO;


import ru.edu.model.UserAccount;

import java.util.List;

/**
 * Класс содержит CRUD методы для работы с базой данных AccountUser- accountuserdb
 */
public class AccountUserService {

    private static final String createTableUserSQL = "CREATE TABLE accountuser( " +
              "login VARCHAR(50) NOT NULL CONSTRAINT accountuser_pk PRIMARY KEY,"+
              "summ REAL NOT NULL," +
              "datebeg BIGINT NOT NULL," +
              "period INT NOT NULL," +
              "percent REAL NOT NULL," +
              "currency VARCHAR(20)NOT NULL);";

    private static final String deleteTableUserSQL = "DROP TABLE accountuser";
    private static final String INSERT_USER_SQL = "INSERT INTO accountuser (login, summ, datebeg, period, percent, currency) VALUES (";
    private static final String SELECT_ALL_USER = "select * from accountuser";
    private static final String SELECT_USER_BY_LOGIN = "select * from accountuser where login = ";
    private static final String DELETE_USER_BY_LOGIN = "delete from accountuser where login = ";


    private AccountDBService accountDBService;

    /**
     * Конструктор для создания объекта AccountDBService и получение внешней зависимости
     * - ссылки на AccountDBService
     * @param accountDBService
     */
    public AccountUserService(AccountDBService accountDBService) {
        this.accountDBService = accountDBService;
    }

    /**
     * Метод создает таблицу persons в БД accountuserdb
     * при выполения SQL запроса createTableUserSQL
     * Возвращает true при нормальном выполнении запроса
     * и возвращает false в случае исключения
     */
    public boolean createTable() {
        this.accountDBService.executeQuery(createTableUserSQL);
        return true;
    }

    /**
     * Метод удаляет таблицу persons в БД accountuserdb
     * при выполения SQL запроса deleteTableUserSQL
     * Возвращает true при нормальном выполнении запроса
     * и false при возникновении исключений
     */
    public boolean deleteTable() {
        this.accountDBService.executeQuery(deleteTableUserSQL);
        return true;
    }

    /**
     * Метод добавляет person в БД accountuserdb, переданного во входном параметре
     * бросает IllegalArgumentException при неверном значении входного параметра
     *
     * добавление сущности accountuser происходит при выполения модифицированного SQL запроса INSERT_USER_SQL
     * Возвращает true при нормальном выполнении запроса
     */
    public boolean addUser(UserAccount user){
        if(user == null){
            return false;
        }
        String sqlInsertUser = INSERT_USER_SQL + "'"
        + user.getLogin()+"' , "
        + user.getSumm() + " , "
        + user.getDateBeg() + " , "
        + user.getPeriod() + " , "
        + user.getPercent() + " , '"
        + user.getCurrency() + "' );";
        this.accountDBService.executeQuery( sqlInsertUser );
        return true;
    }

    /**
     * Метод возвращает список всех accountuser из DB при выполнение запроса SELECT_ALL_USER
     * либо пустой список в случае пустой DB и в случае возникновения исключения
     */
    public List<UserAccount> getAllAccountUser(){
        List<UserAccount> resultList = null;
        boolean resultExecute = this.accountDBService.executeQuery(SELECT_ALL_USER);
        if(resultExecute) {
            resultList = this.accountDBService.getResultList();
        }
        return resultList;
    }

    /**
     * Метод возвращает список AccountUser из DB при выполнение запроса SELECT_USER_BY_LOGIN
     * в качестве входного параметра передается имя человека, по которому производиться выборка
     *
     * возвращает:
     * null в случае отсутствия данных и в случае возникновения исключения
     * UserAccount, удовлетворяющих запросу
     */
    public UserAccount getAccountUserByLogin(String login) {
        String resultSQL = SELECT_USER_BY_LOGIN + " '" + login.toUpperCase() + "'";
        List<UserAccount> resultList = null;
        boolean resultExecute = this.accountDBService.executeQuery(resultSQL);
        if(resultExecute) {
            resultList = this.accountDBService.getResultList();
            return resultList.get(0);
        }
        return null;
    }

     /**
     * Метод удаляет accountUser из DB
     * Возвращает :
     * значение -1 при неверном входном параметре,
     * значение >0 при успешном удалении
     */
    public int deleteAccountUser(String login) {
        if(login == null || login.isEmpty()){
            return -1;
        }
        String resultSQL = DELETE_USER_BY_LOGIN + "'"+login.toUpperCase()+"';";
        int result = this.accountDBService.executeUpdate(resultSQL);
        return result;
    }
}
