package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.model.UserAccount;
import ru.edu.serviceDAO.AccountUserService;

import java.util.Date;

/**
 * Класс осуществляет запросы к ДБ
 */
@Controller
@RequestMapping
public class AccountUserController {

        private AccountUserService accountUserService;

        @Autowired
        public void setUsers(AccountUserService users) {
            this.accountUserService = users;
        }

    /**
     * метод accountUserForm обрабатывает запрос от браузера с мэппингом /add GET запроса
     * по добавлению нового вклада клиента
     *
     */
    @RequestMapping( value="/add",method = RequestMethod.GET)
        public ModelAndView accountUserForm(){
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("/login.jsp");
            modelAndView.addObject("todaydata", new Date().toString());
            return modelAndView;
        }

    /**
     * метод accountUserPost обрабатывает POST запроса с мэппингом /add
     * извлекает из переданной формы login данные по новому вкладу
     * добавляет userAccount в БД и извещает о выполненной транзакции
     *
     */
        @RequestMapping(value="/add", method = RequestMethod.POST)
        public ModelAndView accountUserPost(@RequestParam("login") String login,
                                     @RequestParam("summ") String summ,
                                     @RequestParam("period") String period,
                                     @RequestParam("percent") String percent,
                                     @RequestParam("currency") String currency
                                     ){
            UserAccount userAccount = new UserAccount(login,
                                        Double.parseDouble(summ),
                                        new Date().getTime(),
                                        Integer.parseInt(period),
                                        Double.parseDouble(percent),
                                        currency);

            ModelAndView modelAndView = new ModelAndView();
            if(accountUserService.addUser(userAccount)) {
                modelAndView.setViewName("/add.jsp");
            }else{
                modelAndView.setViewName("/error.jsp");
            }
            return modelAndView;
        }

    /**
     * метод accountUserTestForm обрабатывает запрос от браузера с мэппингом /test GET запроса
     * по проверке накоплений по вкладу для данного UserAccount
     *
     */
    @RequestMapping( value="/test",method = RequestMethod.GET)
    public ModelAndView accountUserTestForm(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/test.jsp");
        modelAndView.addObject("todaydata", new Date().toString());
        return modelAndView;
    }

    /**
     * метод accountUserTestPost обрабатывает POST запроса с мэппингом /test
     * извлекает по полученному login данные по вкладу UserAccount
     * и расчитывает накоплений по вкладу UserAccount и отображает отчет на странице браузера
     */
    @RequestMapping(value="/test", method = RequestMethod.POST)
    public ModelAndView accountUserTestPost(@RequestParam("login") String login){

        UserAccount userAccount = accountUserService.getAccountUserByLogin(login);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("todaydata", new Date().toString());
        modelAndView.addObject("login", userAccount.getLogin());
        Double summ = userAccount.getSumm();
        modelAndView.addObject("summ", summ);
        Integer period = userAccount.getPeriod();
        modelAndView.addObject("period", period);
        modelAndView.addObject("percent", userAccount.getPercent());
        modelAndView.addObject("currency", userAccount.getCurrency());
        modelAndView.addObject("userdate", new Date(userAccount.getDateBeg()).toString());

        long difdate = new Date().getTime() - userAccount.getDateBeg();
        int colmonth= new Date(difdate).getMonth();
        Double depossum = colmonth * (summ / period) ;

        modelAndView.addObject("depossum", depossum.toString());

        if(userAccount != null) {
            modelAndView.setViewName("/testreport.jsp");
        }else{
            modelAndView.setViewName("/error.jsp");
        }
        return modelAndView;
    }

    /** закрытие вклада */
    /**
     * метод accountUserCloseForm обрабатывает запрос от браузера с мэппингом /close GET запроса
     * по закрытию накоплений по вкладу для данного UserAccount
     */
    @RequestMapping( value="/close",method = RequestMethod.GET)
    public ModelAndView accountUserCloseForm(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/test.jsp");
        modelAndView.addObject("todaydata", new Date().toString());
        return modelAndView;
    }

    /**
     * метод accountUserClose обрабатывает запрос от браузера с мэппингом /close POST запроса
     * по закрытию вклада для данного UserAccount - удаляет информацию из accountuserdb
     *
     */
    @RequestMapping( value="/close",method = RequestMethod.POST)
    public ModelAndView accountUserClose(@RequestParam("login") String login){
        ModelAndView modelAndView = new ModelAndView();
        UserAccount userAccount = accountUserService.getAccountUserByLogin(login);
        if(userAccount == null){
            modelAndView.setViewName("/error.jsp");
        }else {
            modelAndView.setViewName("/close.jsp");
            modelAndView.addObject("todaydata", new Date().toString());
            modelAndView.addObject("login", userAccount.getLogin());
            modelAndView.addObject("summ", userAccount.getSumm());
            accountUserService.deleteAccountUser(login);
        }
        return modelAndView;
    }

}
