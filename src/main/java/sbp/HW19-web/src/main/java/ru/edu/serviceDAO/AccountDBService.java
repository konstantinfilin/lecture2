package ru.edu.serviceDAO;

import org.springframework.beans.factory.annotation.Value;
import ru.edu.model.UserAccount;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Класс AccountDBService служит для соедиение с БД и выполнения внешних запросов к БД
 *
 */
public class AccountDBService {


    /** значения для подключения к БД Postgres */
        /** внедряем с помощью setter-injection */

        private String url;
        private String user;
        private String password;

        @Value("${url}")
        public void setUrl(String url) {
            this.url = url;
        }
        @Value("${user}")
        public void setUser(String user) {
            this.user = user;
        }
        @Value("${password}")
        public void setPassword(String password) {
            this.password = password;
        }


        private ExecutorService executorService ;
        private static final int threadPoolNumber = 5;
        private List<UserAccount> resultList = null;

        public AccountDBService(){
            this.executorService = Executors.newFixedThreadPool(threadPoolNumber);
        }


        /**
         * Метод возращает List<UserAccount>, полученный в результате выполнения SQL запроса Select
         */
        public List<UserAccount> getResultList(){
            return this.resultList;
        }


        /**
         * Потокобезопасный метод выполняет SQL запрос, передаваемый во входном параметре str/
         *
         * Если запрос Select отработал, то возвращает true и
         * в resultList присваивается ссылка на список найденных значений
         * при всех остальных запросах - возвращает false
         *
         * В случае возникновения исключения Exception
         * логирует StackTrace и возвращает false
         */
        public boolean executeQuery(String str) {
            if(str == null || str.isEmpty()){
                return false;
            }
            resultList = null;
            try(Connection connection = DriverManager.getConnection(url, user, password);
                Statement statement = connection.createStatement()) {

                Callable<Boolean> executeTask = () -> statement.execute(str);

                Boolean resultStatament = executorService.submit(executeTask).get();

                if (resultStatament) {
                    resultList = Collections.synchronizedList(new ArrayList<>());
                    ResultSet rs = statement.getResultSet();
                    while (rs.next()) {
                        UserAccount tmp = new UserAccount(
                                rs.getString("login"),
                                rs.getDouble("summ"),
                                rs.getLong("datebeg"),
                                rs.getInt("period"),
                                rs.getDouble("percent"),
                                rs.getString("currency"));

                        resultList.add(tmp);
                    }
                }
            }catch (Exception e){
                System.out.println("executeQuery method exception: " + e.getMessage());
                e.printStackTrace();
                return false;
            }
            return true;
        }


        /**
         * Потокобезопасный метод выполняет SQL запрос, передаваемый во входном параметре str
         * бросает IllegalArgumentException при неверном значении параметра str
         *
         * Возвращает :
         * кол-во изменненных записей DB при успешном выполение SQL запроса
         * значение 0 в остальных случаях
         *
         * В случае возникновения исключения Exception
         * логирует StackTrace и возвращает -1
         */
        public synchronized int executeUpdate(String str){
            if(str == null || str.isEmpty()){
                throw new IllegalArgumentException("executeUpdate() method input fail : " + str );
            }
            int result ;
            try ( Connection connection = DriverManager.getConnection(url, user, password);
                  Statement statement = connection.createStatement() ){

                Callable<Integer> executeTask = ()-> statement.executeUpdate(str);
                result = executorService.submit(executeTask).get();

            } catch (Exception e) {
                System.out.println("executeUpdate() method exception : " + e.getMessage());
                e.printStackTrace();
                return -1;
            }
            return result;
        }

}
