package ru.edu.config;


import org.springframework.context.annotation.*;
import ru.edu.serviceDAO.AccountDBService;
import ru.edu.serviceDAO.AccountUserService;


@Configuration
@ComponentScan("ru.edu")
@PropertySource("db.properties")
public class AppConfig
{

    @Bean
    public AccountDBService dbService()
    {
        return new AccountDBService();
    }


    @Bean
    public  AccountUserService DAOAccountUser()
    {
        return new AccountUserService(dbService());
    }

}
