package ru.edu.model;

import java.util.Date;

/**
 * Владелец вклада
 * Вид валюты вклада
 * Сумма вклада
 * Дата открытия вклада
 * Годовой процент по вкладу
 * Сумма начисления за год
 * Дата закрытия вклада
 * Итоговая Сумма с учетом начисления процентов
 */
public class UserAccount  {
    private String login;
    private Double summ;
    private Long dateBeg;
    private Integer period;
    private Double percent;
    private String currency;


    public UserAccount (){}

    public UserAccount (String login, Double summ, Long dateBeg, Integer period, Double percent, String currency) {
        this.login = login.toUpperCase();
        this.summ = summ;
        this.dateBeg = dateBeg;
        this.period = period;
        this.percent = percent;
        this.currency = currency;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public String getLogin() {
        return login;
    }

    public Double getSumm() {
        return summ;
    }

    public Long getDateBeg() {
        return dateBeg;
    }

    public void setDateBeg(Long dateBeg) {
        this.dateBeg = dateBeg;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getCurrency() {
        return currency;
    }

    public void setLogin(String login) {
        this.login = login.toUpperCase();
    }

    public void setSumm(Double summ) {
        this.summ = summ;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "login='" + login + '\'' +
                ", summ=" + summ +
                ", dateBeg=" + new Date(dateBeg) +
                ", period=" + period +
                ", percent=" + percent +
                ", currency='" + currency + '\'' +
                '}';
    }
}
