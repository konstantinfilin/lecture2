package sbp.user;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author k.filin
 *
 * Неизменяемый сlass User - описывает члена фитнес клуба
 */

public class User implements Comparable
{

    private String name;
    private String surname;
    private int age ;
    private int weight;
    private int height;
    private String address;


    private User(){}


    public User(User curUser)
    {
        this.name = curUser.name;
        this.surname = curUser.surname;
        this.age = curUser.age;
        this.weight = curUser.weight;
        this.height = curUser.height;
        this.address = curUser.address;
    }

    public User(String name, String surname, int age, int weight, int height, String address)
    {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.address = address;
    }

    public String getName() { return this.name; }
    public String getSurname() { return this.surname; }
    public int getAge() { return this.age; }
    public int getWeight() { return this.weight; }
    public int getHeight() { return this.height; }
    public String getAddress() { return this.address; }

    @Override
    public boolean equals(Object o)
    {
        if (this == o){
            return true;
        }
        if (o == null || this.getClass() != o.getClass()){
            return false;
        }
        User user = (User) o;
        return this.age == user.age && this.weight == user.weight && this.height == user.height
                && this.name.equalsIgnoreCase(user.name)
                && this.surname.equalsIgnoreCase(user.surname)
                && this.address.equalsIgnoreCase(user.address);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(this.name, this.surname, this.age, this.weight, this.height, this.address);
    }

    @Override
    public String toString()
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("User{ name='")
                .append(this.name)
                .append("' , surname='")
                .append(this.surname)
                .append( "' , age=")
                .append(this.age)
                .append("', weight=")
                .append(this.weight)
                .append(", height=")
                .append(this.height)
                .append(", address='")
                .append(this.address)
                .append( "'}");
        return stringBuffer.toString();
    }

    @Override
    public int compareTo(Object o) {
        if(this == o) return 0;
        if(!(o instanceof User)){
            throw new IllegalArgumentException("User compareTo : no User object");
        }
        User tmpUser = (User) o;
        return Comparator.comparing(User::getName)
                        .thenComparing(User::getSurname)
                        .compare(this, tmpUser);
    }
}