package sbp.collections;

import org.junit.jupiter.api.Test;
import sbp.exceptions.MyClassException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CustomArrayImplTest {

    /**
     * проверка метода size() на равенство 0
     * создаем объект с пустым кол-вом элементов данных
     */
    @Test
    void equal0_size_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertEquals(0, customArray.size());
    }

    /**
     * проверка метода size() на равенство 5
     * создаем объект с 5 элементами данных
     */
    @Test
    void equal5_size_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);
        customArray.add(5);
        assertEquals(5, customArray.size());
    }

    /**
     * проверка метода isEmpty() на равенство true
     * создаем объект с пустым кол-вом элементов данных
     */
    @Test
    void equalsTrue_isEmpty_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertEquals(true, customArray.isEmpty());
    }

    /**
     * проверка метода isEmpty() на равенство false
     * создаем объект с ненулевым кол-вом элементов данных
     */
    @Test
    void equalsFalse_isEmpty_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(1);
        assertEquals(false, customArray.isEmpty());
    }

    /**
     * проверка метода add() - бросает исключение IllegalArgumentException
     * добавляем элемент null в объект CustomArrayImpl
     */
    @Test
    void throwIllegalArgumentException_add_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertThrows(IllegalArgumentException.class, ()-> customArray.add(null));
    }

    /**
     * проверка метода add() - возвращает true
     * добавляем элемент 1 в объект CustomArrayImpl
     */
    @Test
    void eqaulsTrue_add_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertEquals(true, customArray.add(1));
    }

    /**
     * проверка метода addAll(T[] item) - бросает исключение IllegalArgumentException
     * добавляем пустую ссылку на массив в объект CustomArrayImpl
     */
    @Test
    void throwIllegalArgumentException_addAll_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] integers = null;
        assertThrows(IllegalArgumentException.class, ()-> customArray.addAll(integers));
    }

    /**
     * проверка метода addAll(T[] item) - возвращает true
     * добавляем массив целых чисел в объект CustomArrayImpl
     */
    @Test
    void addIntegerArray_addAll_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] integers = new Integer[]{10,20,30};
        assertEquals(true, customArray.addAll(integers));
    }

    /**
     * проверка метода addAll(Collection<T> items) - бросает исключение IllegalArgumentException
     * добавляем пустую ссылку на коллекцию в объект CustomArrayImpl
     */
    @Test
    void ThrowIllegalArgumentException_AddAllCollection_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        List<Integer> integerList = null;
        assertThrows(IllegalArgumentException.class, ()-> customArray.addAll(integerList));
    }

    /**
     * проверка метода addAll(Collection<T> items) - возвращает ture
     * добавляем ссылку на коллекцию в объект CustomArrayImpl
     */
    @Test
    void addCollection_AddAllCollection_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        List<Integer> integerList = Arrays.asList(new Integer[]{10,20,30});
        assertEquals(true, customArray.addAll(integerList));
    }

    /**
     * проверка метода addAll(int index, T[] items) - бросает исключение ArrayIndexOutOfBoundsException
     * добавляем массив в пустой объект CustomArrayImpl
     */
    @Test
    void ThrowArrayIndexOutOfBoundsException_AddAllInsert_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] integers = new Integer[]{10,20,30};
        assertThrows(ArrayIndexOutOfBoundsException.class, ()-> customArray.addAll(1,integers));
    }

    /**
     * проверка метода addAll(int index, T[] items) - возвращает true
     * добавляем массив в непустой объект CustomArrayImpl
     */
    @Test
    void ThrowIllegalArgumentException_AddAllInsert_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] integers = new Integer[]{10,20,30};;
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);
        assertEquals(true, customArray.addAll(1,integers));
    }

    /**
     * проверка метода addAll(int index, T[] items) - бросает исключение IllegalArgumentException
     * добавляем пустую ссылку на массив в объект CustomArrayImpl
     *
     */
    @Test
    void equalsTrue_AddAllInsert_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        Integer[] integers = null;
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);
        customArray.add(4);
        assertThrows(IllegalArgumentException.class, ()-> customArray.addAll(1,integers));
    }


    /**
     * проверка метода T get(int index) - бросание исключения ArrayIndexOutOfBoundsException
     *
     *  в качестве входного параметра index передаем индекс несущесвующего в массиве элемента
     */
    @Test
    void throwArrayIndexOutOfBoundsException_get_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertThrows(ArrayIndexOutOfBoundsException.class, ()-> customArray.get(1));
    }

    /**
     * проверка метода T get(int index) - возвращает 10 - значение по индексу 1
     *
     *  в качестве входного параметра index передаем 1
     */
    @Test
    void getElement1_get_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(20);
        customArray.add(10);
        assertEquals(10, customArray.get(1));

    }

    /**
     * проверка метода T set(int index, T item) - бросает исключение ArrayIndexOutOfBoundsException
     *
     * в качестве index передаем индекс несущесвующего в массиве элемента
     */
    @Test
    void throwArrayIndexOutOfBoundsException_set_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertThrows(ArrayIndexOutOfBoundsException.class, ()-> customArray.set(1,10));
    }

    /**
     * проверка метода T set(int index, T item) - возвращает старое значение 10
     *
     * в качестве нового значения передаем -10
     */
    @Test
    void OldElement10_set_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(20);
        assertEquals(10, customArray.set(1,-10));
        assertEquals(-10, customArray.get(1));
    }

    /**
     * проверка метода T set(int index, T item) - изменение значения по index 1 на 20
     *
     * в качестве нового значения 20, возвращает старое значение 10
     */
    @Test
    void NewElement20_set_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(10, customArray.set(1,20));
        assertEquals(20, customArray.get(1));
    }

    /**
     * проверка метода remove(int index) - бросает исключение ArrayIndexOutOfBoundsException
     *
     * в качестве index передаем индекс несущесвующего в массиве элемента
     */
    @Test
    void throwArrayIndexOutOfBoundsException_remove_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertThrows(ArrayIndexOutOfBoundsException.class, ()-> customArray.remove(-1));
    }

    /**
     * проверка метода remove(int index) - уменьшение кол-ва элементов в массиве
     *
     * создаем массив с 3 элементами,
     * применяем метод remove() к первому элементу: remove(1)
     * кол-во элементов в массиве стало 2
     */
    @Test
    void decrementCountElementsOn1_remove_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(3, customArray.size());
        customArray.remove(1);
        assertEquals(2, customArray.size());
    }

    /**
     * проверка метода remove(int index) - удаление элемента по индексу 1 в массиве
     *
     * создаем массив с 3 элементами,элемент по индексу 1 равен 10
     * применяем метод remove() к элементу по индексу 1: remove(1)
     * элемент по индксу 1 не равен 10
     */
    @Test
    void delete10_remove_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(10, customArray.get(1));
        customArray.remove(1);
        assertFalse(customArray.get(1) == 10);
    }

    /**
     * проверка метода remove(T item) - возвращает false
     *
     * в качестве item передаем неверный элемент null
     */
    @Test
    void returnFalse_remove_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        assertEquals(false, customArray.remove(null));
    }

    /**
     * проверка метода remove(T item) - возвращает true
     *
     * в качестве item передаем элемент 10
     */
    @Test
    void returnTrue10_remove_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(true, customArray.remove((Integer) 10));
    }

    /**
     * проверка метода remove(T item) - возвращает false
     *
     * в качестве item передаем несуществующий элемент в масиве
     */
    @Test
    void returnFalse100_remove_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(false, customArray.remove((Integer) 100));
    }

    /**
     * проверка метода contain(T item) - возвращает true
     *
     * в качестве item передаем значение 10
     */
    @Test
    void returnTrue10_contain_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(true, customArray.contains(10) );
    }

    /**
     * проверка метода contain(T item) - возвращает false
     *
     * в качестве item передаем значение 100
     */
    @Test
    void returnFalse100_contain_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(false, customArray.contains(100) );
    }


    /**
     * проверка метода indexOf(T item) - возвращает индекс 2
     *
     * в качестве item передаем значение 30
     */
    @Test
    void return2_indexOf_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(2, customArray.indexOf(30) );
    }

    /**
     * проверка метода indexOf(T item) - возвращает -1
     *
     * в качестве item передаем значение 130, не существующее в массиве
     */
    @Test
    void returnMinus1_indexOf_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(-1, customArray.indexOf(130) );
    }

    /**
     * проверка метода ensureCapacity(i)  и getCapacity() - увеличим объем массива
     *
     * создаем массив с объемом 10
     * применяем метод ensureCapacity(100)
     * объем массива становиться 100
     */
    @Test
    void increment100_ensureCapacity_test() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>(10);
        customArray.add(0);
        customArray.add(10);
        customArray.add(30);
        assertEquals(10, customArray.getCapacity());
        customArray.ensureCapacity(100);
        assertEquals(100, customArray.getCapacity());
    }

    /**
     * проверка метода reverse() и toArray() - меняем порядок во входном массиве
     *
     * создаем массив с элементами 10,20,
     * вызываем метод reverse,
     * сравниваем с заготовленным результатом 20,10
     */
    @Test
    void reverse() {
        CustomArrayImpl<Object> customArray = new CustomArrayImpl<>(10);
        Object[] input = new Object[]{10,20};
        Object[] inputTrue = new Object[]{20,10};
        customArray.addAll(input);
        customArray.reverse();
        Object[] output = customArray.toArray();
        assertArrayEquals(inputTrue,output);
    }

}