package sbp.collections;

import java.util.Comparator;
import java.lang.Integer;


/**
 * Класс CustomDigitComparator реализует сравнение целых чисел
 * на основе интерфейса Comparator<>
 * @author Константин Филин
 * @version 1.0
 */
public class CustomDigitComparator implements Comparator<Integer> {

    /**
     * Функция определяет порядок между двумя целыми числами integer1 и integer2
     * Сортирует сначала четные числа, затем нечетные;
     * На вход подаются числа integer1 и integer2, отличные от null;
     * @return возвращает целое число :
     * -1 - четное число integer1 меньше четного числа integer2
     * 0 - integer1 и integer2 - четные
     * 1 - четное число integer1 большего четного числа integer2
     * @throws IllegalArgumentException - бросает исключение при входных параметрах null
     */
    @Override
    public int compare(Integer integer1, Integer integer2) {
        if(integer1 == null || integer2 == null) {
            throw new IllegalArgumentException("CustomDigitComparator : compare method argument's is null");
        };

        if (integer1 % 2 == integer2 % 2) {
            return integer1.compareTo(integer2);
        }
        return (integer1 % 2 < integer2 % 2) ? -1 : 1;
    }
}