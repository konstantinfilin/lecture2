package sbp.collections;

import java.util.Arrays;
import java.util.Collection;

/**
 * Класс CustomArrayImpl создает объект, аналогичные ArrayList<>
 * с переопределением методов
 *
 * @param <T>
 */
public class CustomArrayImpl<T> implements CustomArray<T> {
    /** поле емкости массива данных по умолчанию */
    private final static int DEFAULT_CAPACITY = 10;
    /** поле массива данных */
    private T[] dataArray;
    /** поле содержащее кол-во элементов в массиве данных */
    private int countElements;

    /**
     * Конструктор создает новый массив элементов емкостью DEFAULT_CAPACITY
     */
    public CustomArrayImpl(){
        this.dataArray = (T[]) new Object[DEFAULT_CAPACITY];
        this.countElements = 0;
    }

    /**
     * Конструктор создает новый массив элементов емкостью capacity
     */
    public CustomArrayImpl(int capacity) {
        if ( capacity <= 0 ) {
            throw new IllegalArgumentException( "CustomArrayImpl constactor: capacity is not correct");
        }
            this.dataArray = (T[]) new Object[capacity];
            this.countElements = 0;
    }

    /**
     * Конструктор создает новый массив элементов из элементов коллекции
     */
    public CustomArrayImpl(Collection<T> c){
        if(c == null){
            throw new IllegalArgumentException( "CustomArrayImpl constactor: collection is null");
        }
        this.dataArray = (T[]) c.toArray();
        this.countElements = this.dataArray.length;
    }


    /**
     * Метод возвращает кол-во элементов в массиве данных
     */
    @Override
    public int size() {
        return this.countElements;
    }

    /** метод возвращает true, если массив данных пуст */
    @Override
    public boolean isEmpty() {
        return this.countElements == 0;
    }

    /** метод возвращает true, если в массив данных добавлен новый элемент
    * @throws IllegalArgumentException
    */
    @Override
    public boolean add(T item) {
        if(item == null){
            throw new IllegalArgumentException( "add items is null");
        }
        ensureCapacity(this.size() + 1);
        this.set(this.size(), item);
        ++this.countElements;
        return true;
    }

    /**
     * метод добавляет массив элементов в массив данных, увеличивая, если необходимо, емкость массива данных
     * бросает исключение, если items равен null
     * @throws IllegalArgumentException
     */
    @Override
    public boolean addAll(T[] items) {
        if(items == null){
            throw new IllegalArgumentException( "items is null");
        }
        int curCountElements = this.size() + items.length;
        ensureCapacity(curCountElements);
        T[] newdataArray = (T[]) new Object[this.getCapacity()];

        System.arraycopy(this.dataArray, 0 , newdataArray, 0, this.size());
        System.arraycopy(items, 0 ,  newdataArray, this.size() , items.length);

        this.dataArray = newdataArray;
        this.countElements = curCountElements;
        newdataArray = null;
        return true;
    }

    /**
     * метод добавляет элементы в коллекцию
     * бросает исключение если items равен null
     * @throws IllegalArgumentException
     */
    @Override
    public boolean addAll(Collection<T> items) {
        if(items == null){
            throw new IllegalArgumentException( "addAll Collection: items is null");
        }
        T[] arrItems = ( T[] ) items.toArray();
        return addAll(arrItems);
    }

    /**
     * метод добавляет элементы в коллекцию, начиная с элемента index
     * бросает исключение IllegalArgumentException, если items равен null
     * бросает исключение ArrayIndexOutOfBoundsException, если index не верный
     *
     * @param index - index
     * @param items - items for insert
     * @throws ArrayIndexOutOfBoundsException
     * @throws IllegalArgumentException
     */
    @Override
    public boolean addAll(int index, T[] items) {
        if( index < 0 || index > this.size() ){
            throw new ArrayIndexOutOfBoundsException("index is out of bounds : " + index);
        }
        if(items == null){
            throw new IllegalArgumentException("items is null");
        }

        int curCountElements = this.size() + items.length;
        ensureCapacity(curCountElements);
        T[] newdataArray = (T[]) new Object[this.getCapacity()];

        System.arraycopy(this.dataArray, 0 , newdataArray, 0 , index);
        System.arraycopy(items, 0 ,  newdataArray, index , items.length);
        System.arraycopy(this.dataArray, index , newdataArray, index + items.length, this.size() - index);

        this.dataArray = newdataArray;
        this.countElements = curCountElements;
        newdataArray = null;
        return true;
    }

    /**
     * метод возращает элемент массива данных по index,
     * бросает исключение, если index выходит за педелы диапазона
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException
     */
    @Override
    public T get(int index) {
        if(index < 0 || index > this.size()){
            throw new ArrayIndexOutOfBoundsException("index is out of bounds : " + index);
        }
        return this.dataArray[index];
    }

    /**
     * метод заменяет элемент по index в массиве данных на item
     *
     * @param index - index
     * @return old value
     *  бросает исключение, если index не верный
     * @throws ArrayIndexOutOfBoundsException
     */
    @Override
    public T set(int index, T item) {
        if(index < 0 || index > this.size()){
            throw new ArrayIndexOutOfBoundsException("index is out of bounds : " + index);
        }
        T oldItem = this.get(index);
        this.dataArray[index] = item;
        return oldItem;
    }

    /**
     * Удаляем элемент из массива данных по index.
     *
     * @param index - index
     * Бросает исключениие, если индех не верный
     * @throws ArrayIndexOutOfBoundsException
     */
    @Override
    public void remove(int index) {
        if( this.size() > 0 ) {
            if (index < 0 || index > this.size()) {
                throw new ArrayIndexOutOfBoundsException("Remove: index is out of bounds : " + index);
            }
            T[] tmp = this.dataArray;
            this.dataArray = (T[]) new Object[tmp.length - 1];
            System.arraycopy(tmp, 0, this.dataArray, 0, index);
            int lastIndex = tmp.length - index - 1;
            System.arraycopy(tmp, index + 1, this.dataArray, index, lastIndex);
            --this.countElements;
            tmp = null;
        }
    }

    /**
     * Метод удаляет первый встреченный элемент item, начиная с начала массива элементов.
     *
     * @param item - item
     * @return true if item was removed
     */
    @Override
    public boolean remove(T item) {
        if(item == null){
            return false;
        }
        int index = this.indexOf(item);
        if(index >= 0) {
            this.remove(index);
            return true;
        }else{
            return false;
        }
    }

    /**
     * Метод возвращает true, если элемент item обнаружен в массиве данных
     * @param item - item
     */
    @Override
    public boolean contains(T item) {
        if( indexOf(item) >= 0 ){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Метод возвращает индекс элемента в массиве данных, если элемент item обнаружен в нем
     * иначе возвращает -1
     * @param item - item
     */
    @Override
    public int indexOf(T item) {
        if (item == null) {
            return -1;
        }
        for(int i = 0 ; i < this.size(); ++i){
            if(this.dataArray[i].equals(item)){
                return i;
            }
        }
        return -1;
    }

    /**
     * метод увеличивает емкость массива данных, если это требуется
     * @param newElementsCount
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        if( newElementsCount  > this.getCapacity() ){
            int newCapacity = Math.max(( (this.getCapacity() * 3) / 2 + 1) , newElementsCount );
            T[] newdataArray = (T[]) new Object[newCapacity];
            System.arraycopy(this.dataArray, 0 , newdataArray, 0, this.size());
            this.dataArray = newdataArray;
            newdataArray = null;
       }
    }

    /** метод возвращает емкость массива данных */
    @Override
    public int getCapacity() {
        return this.dataArray.length;
    }

    /**
     * метод изменяет порядок массива данных на обратный
     */
    @Override
    public void reverse() {
        for(int i = 0, j = this.size() - 1; i<=j; ++i, --j){
            T tmp = this.dataArray[i];
            this.dataArray[i] = this.dataArray[j];
            this.dataArray[j] = tmp;
        }

    }

    /**
     * Метод возвращает массив Object, созданный из массива данных
     */
    @Override
    public Object[] toArray() {
        return Arrays.copyOf(this.dataArray,this.size());
    }

    @Override
    public String toString(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("CustomArray [ ").append(this.size()).append(" ]{ ");
        for(int i = 0 ; i < this.size(); ++i){
            stringBuffer.append(this.get(i));
            if(i != this.size() - 1) stringBuffer.append(" , ");
        }
        stringBuffer.append(" }");
        return stringBuffer.toString();
    }
}
