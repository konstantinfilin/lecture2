package sbp.collections;

import sbp.person.Person;
import java.util.Comparator;

/**
 * Класс SortPersonByNameComparator реализует сравнение объектов Person
 * по имени человека на основе интерфейса Comparator<>
 * @author Константин Филин
 * @version 1.0
 */
public class SortPersonByNameComparator implements Comparator<Person> {

    /**
     * Функция сравнения объектов Person по имени в лексикографическом порядке
     * @return возвращает целое число :
     * меньше 0 - именя в person1 меньше имени у person2
     * 0 - названия имен совпадают
     * больше 0 - именя в person1 больше имени у person2
     * @throws IllegalArgumentException - бросает исключение при входных параметрах null
     */
    @Override
    public int compare(Person person1, Person person2) {
        if(person1 == null || person2 == null){
            throw new IllegalArgumentException("SortPersonByNameComparator: compare method argument's is null");
        }
        return person1.getName().compareTo( person2.getName() );
    }

}
