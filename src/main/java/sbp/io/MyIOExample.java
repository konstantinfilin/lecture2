package sbp.io;

import java.io.*;
import java.util.Date;

public class MyIOExample
{

    /**
     * Создать объект класса {@link File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName)
    {
        File inputFile = new File(fileName);
        if(inputFile.exists())
        {
            System.out.println("Абсолютный путь : " + inputFile.getAbsoluteFile());
            System.out.println("Родительский путь : " + inputFile.getParent());

            if (inputFile.isFile())
            {
                System.out.println("Файл : " + fileName);
                System.out.println("Размер файла : " + inputFile.length());
                Date date = new Date(inputFile.lastModified());
                System.out.println("Время последнего изменения файла : " + date);
            }else if(inputFile.isDirectory())
            {
                System.out.println("Директория : " + fileName);
            }
            return true;
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileInputStream} и {@link FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws Exception
    {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(sourceFileName);
            outputStream = new FileOutputStream(destinationFileName);
            byte[] buffer = new byte[1024];

            int length = inputStream.read(buffer);
            while (length  > 0)
            {
                outputStream.write(buffer, 0, length);
                length = inputStream.read(buffer);
            }
        } catch (IOException ex)
        {
            System.out.println("Error in copyFile : "+ ex.getMessage());
            ex.printStackTrace();
        }
        finally
        {
            inputStream.close();
            outputStream.close();
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link BufferedInputStream} и {@link BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName)  {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        try
        {
             fileInputStream = new FileInputStream(sourceFileName);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
             fileOutputStream = new FileOutputStream(destinationFileName);
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

             int curInputByte;
             while ( ( curInputByte = bufferedInputStream.read() ) != -1 )
             {
                bufferedOutputStream.write(curInputByte);
             }
        }catch (Exception ex)
        {
            try
            {
                fileInputStream.close();
            } catch (Exception exception)
            {
                System.out.println("Error in copyBufferedFile : "+ ex.getMessage());
                exception.addSuppressed(ex);
                exception.printStackTrace();
            }

            try
            {
                fileOutputStream.close();
            } catch (Exception exception)
            {
                System.out.println("Error in copyBufferedFile : "+ ex.getMessage());
                exception.addSuppressed(ex);
                exception.printStackTrace();
            }
            return false;
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileReader} и {@link FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName)
    {

        try ( FileReader fileReader =  new FileReader(sourceFileName);
              FileWriter fileWriter = new FileWriter(destinationFileName) )
        {
            int curChar;
            while( (curChar = fileReader.read()) != -1 ){
                fileWriter.write(curChar);
            }
            fileWriter.flush();

        }catch (Exception ex){
            System.out.println("Error in copyFileWithReaderAndWriter : "+ ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        return true;
    }
}