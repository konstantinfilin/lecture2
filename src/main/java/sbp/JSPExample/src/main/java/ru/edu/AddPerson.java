package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Данный класс создает Servlet, добавляет нового гостя
 * Для добавления гостя в методе doGet вызывается  http://localhost:8080/JSPExample/addperson
 *
 * После внесения нового гостя, осуществляется переход на основонй сервлет /JSPExample
 *
 */
@WebServlet("/addperson")
public class AddPerson extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/addperson.html").forward(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html; charset=UTF-8");
        try {
            DBServiceThread dbService = new DBServiceThread();
            String name = req.getParameter("name");
            String city = req.getParameter("city");
            String age = req.getParameter("age");

            if(name.isEmpty() || city.isEmpty() || age.isEmpty()){
                resp.sendRedirect("/JSPExample");
                return;
            }
            int ageInt = Integer.parseInt(age);
            Person person = new Person(name, city, ageInt);
            new PersonDAOThread(dbService).addPerson(person);

            resp.sendRedirect("/JSPExample");

        }catch (Exception e){
            System.out.println("DoPost exception :" + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }
}
