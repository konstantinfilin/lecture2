package ru.edu;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;


/**
 * Данный класс создает Servlet, содержащий описание Разработчика Web-приложения
 * Для получения данной страницй нужно перейти по ссылке: http://localhost:8080/JSPExample/about
 * Для перехода обратно - используйте кнопку "Вернуться обратно"
 *
 */
@WebServlet("/about")
public class SecondPade extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html; charset=UTF-8");

        try(Writer writer = resp.getWriter()) {
            writer.write("<h3>" + "Разработчик Web-приложения :" + "</h3>");
            writer.write("<div>" + "© Веб студия «Бла-плано», разработка сайта, 2022" + "</div>");
            writer.write("<br>");
            writer.write("http://bla-plano.ru/2022/02/21");
            writer.write("<hr>");
            writer.write("<button onclick=\"document.location='/JSPExample'\">Вернуться обратно</button>");
            writer.flush();
        }
    }

}
