package ru.edu.old;


import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class AuthServlet extends HttpServlet {

    private List<UserInfo> users = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        String path = req.getRequestURI();
        if(path.endsWith("/auth")) {
            Cookie userInfoCookie = getUserInfoCookie(req);

            if (userInfoCookie == null) {
                getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
            } else {
                String userInfoJSON = new String(Base64.getDecoder().decode(userInfoCookie.getValue()));

                UserInfo userInfo = new ObjectMapper().readValue(userInfoJSON, UserInfo.class);

                req.setAttribute("name", userInfo.getName());
                req.setAttribute("login", userInfo.getLogin());

                getServletContext().getRequestDispatcher("/info.jsp").forward(req, resp);
            }
        }else if(path.endsWith("/auth/all")){
            req.setAttribute("usersList", users);
            getServletContext().getRequestDispatcher("/all.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        Boolean shouldDelete = Boolean.parseBoolean(req.getParameter("shouldDelete"));
        String status;

        if(!shouldDelete) {

            String login = req.getParameter("login");
            String name = req.getParameter("name");

            UserInfo userInfo = new UserInfo(login, name);
            String infoJson = new ObjectMapper().writeValueAsString(userInfo);

            String encodeInfo = Base64.getEncoder().encodeToString(infoJson.getBytes());

            resp.addCookie(new Cookie("userInfo",encodeInfo ));
            users.add(userInfo);

            status="OK";
        }else{

            Cookie cookie = new Cookie("userInfo","");
            cookie.setMaxAge(0);
            resp.addCookie(cookie);

           status = "DELETED OK";
        }
        req.setAttribute("message", status);
        getServletContext().getRequestDispatcher("/status.jsp").forward(req, resp);
    }

    private Cookie getUserInfoCookie(HttpServletRequest req){
        for(Cookie cookie: req.getCookies()){
            if(cookie.getName().equals("userInfo")){
                return cookie;
            }
        }
        return null;
    }
}

/*
   @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        HttpSession session = req.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");

        if(userInfo == null) {
            Writer write = resp.getWriter();
            write.write("<html>");
            write.write("<body>");
            write.write("Data : " + new Date());
            write.write("<h2>Create new user</h2>");
            write.write("<form method = \"POST\" action = \"\">");
            write.write("<input name=\"shouldDelete\" value=\"false\" hidden>");
            write.write("Login : <input name=\"login\">");
            write.write("Name : <input name=\"name\">");
            write.write("<input type=\"submit\" value=\"Create\">");
            write.write("</form>");
            write.write("</body>");
            write.write("</html>");
        }else{
            Writer write = resp.getWriter();
            write.write("<html>");
            write.write("<body>");
            write.write("Data : " + new Date());
            write.write("<h2>Hello , " + userInfo.getName() + "</h2>");
            write.write("Login :" + userInfo.getLogin());
            write.write("Name :"  + userInfo.getName());
            write.write("<hr>");
            write.write("<form method = \"POST\" action = \"\">");
            write.write("<input name=\"shouldDelete\" value=\"true\" hidden>");
            write.write("<input type=\"submit\" value=\"Delete\">");
        //    write.write("</form>");
            write.write("</body>");
            write.write("</html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Boolean shouldDelete = Boolean.parseBoolean(req.getParameter("shouldDelete"));

        if(!shouldDelete) {

            String login = req.getParameter("login");
            String name = req.getParameter("name");

            UserInfo userInfo = new UserInfo(login, name);

            HttpSession session = req.getSession();

            session.setAttribute("userInfo", userInfo);

            Writer writer = resp.getWriter();
            writer.write("<html><body>OK</body></html>");
        }else{
            HttpSession session = req.getSession();
            session.removeAttribute("userInfo");

            Writer writer = resp.getWriter();
            writer.write("<html><body>DELETED OK</body></html>");
        }
    }
 */