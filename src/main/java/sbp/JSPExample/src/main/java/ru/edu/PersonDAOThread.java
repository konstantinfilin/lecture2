package ru.edu;


import java.util.List;

/**
 * Класс содержит CRUD методы для работы с базой данных Persons
 */
public class PersonDAOThread {

        /**
         * Константы-заготовки для формирования SQL запросов
         */
        private static final String createTablePersonSQL = "CREATE TABLE persons (" +
                "ID SERIAL NOT NULL CONSTRAINT persons_pk PRIMARY KEY," +
                "NAME VARCHAR(50) NOT NULL," +
                "CITY VARCHAR(50) NOT NULL," +
                "AGE INT NOT NULL);";
        private static final String deleteTablePersonSQL = "DROP TABLE persons";
        private static final String INSERT_PERSON_SQL = "INSERT INTO persons (name, city, age ) VALUES (";
        private static final String SELECT_ALL_PERSONS = "select * from persons";
        private static final String SELECT_PERSONS_BY_NAME = "select * from persons where name = ";


        private DBServiceThread dbService;

        /**
         * Конструктор для создания объекта PersonDAOService и получение внешней зависимости
         * - ссылки на DBService
         * @param dbService
         */
        public PersonDAOThread(DBServiceThread dbService) {
            this.dbService = dbService;
        }

        /**
         * Метод создает таблицу persons в БД personsdb
         * при выполения SQL запроса createTablePersonSQL
         * Возвращает true при нормальном выполнении запроса
         * и возвращает false в случае исключения
         */
        public boolean createTable() {
            this.dbService.executeQuery(createTablePersonSQL);
            return true;
        }

        /**
         * Метод удаляет таблицу persons в БД personsdb
         * при выполения SQL запроса deleteTablePersonSQL
         * Возвращает true при нормальном выполнении запроса
         * и false при возникновении исключений
         */
        public boolean deleteTable() {
            this.dbService.executeQuery(deleteTablePersonSQL);
            return true;
        }

        /**
         * Метод добавляет person в БД personsdb, переданного во входном параметре
         * бросает IllegalArgumentException при неверном значении входного параметра
         *
         * добавление сущности persons происходит при выполения модифицированного SQL запроса INSERT_PERSON_SQL
         * Возвращает true при нормальном выполнении запроса
         */
        public boolean addPerson(Person person){
            if(person == null){
                return false;
            }
            String sqlInsertPerson = INSERT_PERSON_SQL + "'"
                    + person.getName() + "' , '"
                    + person.getCity() + "' , "
                    + person.getAge() + " );";
            this.dbService.executeQuery( sqlInsertPerson );
            return true;
        }

        /**
         * Метод возвращает список всех Person из DB при выполнение запроса SELECT_ALL_PERSONS
         * либо пустой список в случае пустой DB и в случае возникновения исключения
         */
        public List<Person> getAllPersons(){
            List<Person> resultList = null;
            boolean resultExecute = this.dbService.executeQuery(SELECT_ALL_PERSONS);
            if(resultExecute) {
                resultList = this.dbService.getResultList();
            }
            return resultList;
        }


        /**
         * Метод возвращает список Person из DB при выполнение запроса SELECT_PERSONS_BY_NAME
         * в качестве входного параметра передается имя человека, по которому производиться выборка
         *
         * возвращает:
         * null в случае отсутствия данных и в случае возникновения исключения
         * List<Person> с person, удовлетворяющих запросу
         */
        public List<Person> getPersonsByName(String name) {
            String resultSQL = SELECT_PERSONS_BY_NAME + " '" + name.toUpperCase() + "'";
            List<Person> resultList = null;
            boolean resultExecute = this.dbService.executeQuery(resultSQL);
            if(resultExecute) {
                resultList = this.dbService.getResultList();
            }
            return resultList;
        }

        /**
         * Метод изменяет город на newCity для человека из DB, чей id равен входному значению update_id
         * в качестве входных параметров передается newCity и update_id, по которому производиться выборка
         * при выполнение  запроса UPDATE
         * Возвращает :
         * значение -1 при неверных входных параметрах,
         * кол-во измененных записей DB при успешном выполение SQL запроса,
         * значение 0 в остальных случаях.
         */
        public int updateCityPerson(String newCity, int update_id) {
            int result = - 1;
            if(newCity == null || newCity.isEmpty() || update_id < 0)
                return result;
            String resultSQL = "update persons set city = '"
                    + newCity.toUpperCase()
                    + "' where id = " + Integer.toString(update_id);
            result = this.dbService.executeUpdate(resultSQL);
            return result;
        }

        /**
         * Метод удаляет person из DB, чей возраст равен входному значению age
         * при выполнение запроса DELETE
         * Возвращает :
         * значение -1 при неверном входном параметре,
         * кол-во удаленных person при успешном выполение SQL запроса,
         * значение 0 в остальных случаях.
         */
        public int deletePersonsByAge(int age) {
            int result = - 1;
            if(age < 0 )
                return result;
            String resultSQL = "delete from persons where age = " + Integer.toString(age);
            result = this.dbService.executeUpdate(resultSQL);
            return result;
        }
}
