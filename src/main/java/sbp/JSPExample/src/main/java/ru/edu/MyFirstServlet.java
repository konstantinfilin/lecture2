package ru.edu;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Данный класс создает Servlet, содержащий описание мероприятия и списком гостей на мероприяти
 * Для получения данной страницы нужно перейти по ссылке: http://localhost:8080/JSPExample
 *
 *  В случае пустого списка гостей, вызывается сервлет /JSPExample/errlist
 *
 */
@WebServlet("/index.html")
public class MyFirstServlet extends HttpServlet {
    String headText ="Самой интересное событие года!";
    String bodyText ="Массовое мероприятие – это одна из самых продуктивных и интересных<br>"+
            "форм библиотечной работы с читателями. Занимаясь подготовкой и<br>" +
            "реализацией массового мероприятия, библиотекарь может проявить себя как<br>" +
            "творческая личность и, вместе с тем, как ответственный организатор, который<br>" +
            "понимает, какие средства нужно использовать и как распределить обязанности.<br>";
    String persons ="На данном мероприятии присутствовали гости :";
    String curDate = "<h4>Current time is ";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        DBServiceThread dbService = new DBServiceThread();
        List<Person> personList = new PersonDAOThread(dbService).getAllPersons();
        if(personList == null){
            resp.sendRedirect("/JSPExample/errlist");
            return;
        }
        resp.setContentType("text/html; charset=UTF-8");

        try (Writer writer = resp.getWriter()) {
            writer.write("<h1>" + headText + "</h1>");
            writer.write("<div>" + bodyText + "</div>");
            writer.write("<br>");
            writer.write("<hr>");
            writer.write("<br>");
            writer.write("<h3>" + persons + "</h3>");
            writer.write("<ul>");
            for (Person p : personList) {
                writer.write("<li>" + "Имя :" + p.getName() + " , Город : " + p.getCity() + " , Возвраст: " + p.getAge() + "</li>");
            }
            writer.write("</ul>");
            writer.write("<br>");
            writer.write("<hr>");
            writer.write(curDate + LocalDateTime.now() + "</h4>");
            writer.write("<hr>");
            writer.write("<div><button onclick=\"document.location='/JSPExample/addperson'\">Добавить гостя</button></div>");
            writer.write("<br><br>");
            writer.write("<div><button onclick=\"document.location='/JSPExample/about'\">О WEB-разработке</button></div>");
            writer.flush();
        }
    }
}
