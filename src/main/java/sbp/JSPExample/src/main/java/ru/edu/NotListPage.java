package ru.edu;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * Данный класс создает Servlet, обрабатывающий пустой списком гостей
 *
 */
@WebServlet("/errlist")
public class NotListPage extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            resp.setContentType("text/html; charset=UTF-8");

            try(Writer writer = resp.getWriter()) {
                writer.write("<h2>" + "Обнаружен пустой список гостей " + "</h2>");
                writer.write("<h3>" + "Обратитесь в службу разработки : " + "</h3>");
                writer.write("<button onclick=\"document.location='/JSPExample/about'\">Служба разарботки</button>");
                writer.flush();
            }

        }

    }
