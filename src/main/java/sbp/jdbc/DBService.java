package sbp.jdbc;

import sbp.person.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс DBService служит для соедиение с БД и выполнения внешних запросов к БД
 *
 */
public class DBService {
        /** константы для подключения к БД Postgres */
        private static final String url = "jdbc:postgresql://localhost:5432/personsdb";
        private static final String user = "postgres";
        private static final String password = "1904";

        private List<Person> resultList= null;




    /**
     * Метод возращает List<Person>, полученный в результате выполнения SQL запроса Select
     */
    public  List<Person> getResultList(){
            return this.resultList;
        }


    /**
     * Метод выполняет SQL запрос, передаваемый во входном параметре str, возвращает false
     *
     * если запрос Select отработал, то возвращает true и
     * в resultList присваивается ссылка на список найденных значений
     *
     * В случае возникновения исключения SQLException,
     * логирует StackTrace и возвращает false
     */
        public boolean executeQuery(String str) throws SQLException {
            if(str == null || str.isEmpty()){
               return false;
            }
            resultList = null;
            try ( Connection connection = DriverManager.getConnection(url, user, password);
                  Statement statement = connection.createStatement()){

                boolean resultStatament = statement.execute(str);
                if(resultStatament) {
                    resultList = new ArrayList<>();
                    ResultSet rs = statement.getResultSet();
                    while (rs.next()) {
                        Person tmpPerson = new Person(rs.getString("name"),
                                rs.getString("city"),
                                rs.getInt("age"));
                        resultList.add(tmpPerson);
                    }
                }
            } catch (SQLException sqlException) {
                System.out.println(sqlException.getMessage());
                sqlException.printStackTrace();
                return false;
            }
            return true;
        }


    /**
     * Метод выполняет SQL запрос, передаваемый во входном параметре str
     * бросает IllegalArgumentException при неверном значении параметра str
     *
     * Возвращает :
     * кол-во изменненных записей DB при успешном выполение SQL запроса
     * значение 0 в остальных случаях
     *
     * В случае возникновения исключения SQLException,
     * логирует StackTrace и возвращает -1
     */
    public int executeUpdate(String str){
        if(str == null || str.isEmpty()){
            throw new IllegalArgumentException("executeUpdate() method input fail : " + str );
        }
        int result ;
        try ( Connection connection = DriverManager.getConnection(url, user, password);
              Statement statement = connection.createStatement() ){

            result = statement.executeUpdate(str);

        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
            sqlException.printStackTrace();
            return -1;
        }
        return result;
    }
}
