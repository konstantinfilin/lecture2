package sbp.jdbc.web;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;
import sbp.jdbc.PersonDAOThread;
import sbp.person.Person;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

/**
 * класс служит для обработки запроса POST /add
 * распознает запрос от клиента и получает данные для добавления новых объектов в БД
 */
public class AddPersonsHandler implements HttpHandler
{
    private final PersonDAOThread dao;

    /**
     * Внедряет внешнюю зависимость PersonDAO
     */
    public AddPersonsHandler(PersonDAOThread dao)
    {
        this.dao = dao;
    }

    /**
     * Обработчик, который вызывается для обработки HTTP Exchange
     * Читает POST /add запрос от клиента для добавления новой person в БД
     * получает person от клиента в виде JSON объекта
     *
     * Бросает IOException в случае ошибки
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        OutputStream outputStream = null;
        try {
            outputStream = exchange.getResponseBody();
            StringBuilder htmlBuilder = new StringBuilder();

            if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
                String jsonPerson =
                        new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                                .lines()
                                .collect(Collectors.joining());

                JSONObject jsonObject = new JSONObject(jsonPerson);

                String name = jsonObject.toMap().get("name").toString();
                String city = jsonObject.toMap().get("city").toString();
                int age = jsonObject.getInt("age");
                Person person = new Person(name, city, age);

                htmlBuilder.append("<html>")
                        .append("<body>")
                        .append("<h1>")
                        .append("Add new person in DB: " + postAddPersonMethod(person))
                        .append("</h1>")
                        .append("</body>")
                        .append("</html>");
            }

            String htmlStr = htmlBuilder.toString();
            exchange.sendResponseHeaders(200, htmlStr.length());
            outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
       }finally {
           if(outputStream != null )
               outputStream.close();
       }
    }


    /**
     * метод добавляет объект Person в БД
     *
     * возвращает строковое сообщение SUCCESS - в случае удачного завершения
     * и false - в случае Exception
     *
     */
    private String postAddPersonMethod(Person person)
    {
        try
        {
            this.dao.addPerson(person);
            return "SUCCESS";
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "false";
        }
    }

}
