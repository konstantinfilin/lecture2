package sbp.jdbc.web;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import sbp.jdbc.PersonDAOThread;
import sbp.person.Person;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;


/**
 * класс служит для обработки запроса GET /persons
 */
public class GetPersonsHandler implements HttpHandler{
    private final PersonDAOThread dao;

    /**
     * Внедряет внешнюю зависимость PersonDAO
     */
    public GetPersonsHandler(PersonDAOThread dao)
    {
        this.dao = dao;
    }


    /**
     * Обработчик, который вызывается для обработки HTTP Exchange
     * Читает GET запрос от клиентаб читает все person в БД
     *
     * Бросает IOException в случае ошибки
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        OutputStream outputStream = null;
        try {
            outputStream = exchange.getResponseBody();
            StringBuilder htmlBuilder = new StringBuilder();

            if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {
                htmlBuilder.append("<html>")
                        .append("<body>")
                        .append("<h1>");

                List<Person> personList = this.dao.getAllPersons();
                personList.forEach(person -> {
                    htmlBuilder.append("<p>")
                            .append(person.toString())
                            .append("</p>");
                });

                htmlBuilder.append("</h1>")
                        .append("</body>")
                        .append("</html>");
            }

            String htmlStr = htmlBuilder.toString();

            exchange.sendResponseHeaders(200, htmlStr.length());

            outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        }finally {
         if(outputStream != null)
             outputStream.close();
        }
    }

}
