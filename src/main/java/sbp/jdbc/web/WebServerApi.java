package sbp.jdbc.web;

import com.sun.net.httpserver.HttpServer;
import sbp.jdbc.DBServiceThread;
import sbp.jdbc.PersonDAOThread;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.concurrent.Executors;

public class WebServerApi {

    private static final String HOSTNAME = "localhost";
    private static final int PORT = 8080;

    public static void main(String[] args) throws SQLException, IOException
    {
        DBServiceThread dbService = new DBServiceThread();
        HttpServer server = HttpServer.create(new InetSocketAddress(HOSTNAME, PORT), 0);

        server.createContext("/persons", new GetPersonsHandler( new PersonDAOThread(dbService)) );
        server.createContext("/add", new AddPersonsHandler(new PersonDAOThread(dbService)) );
        server.createContext("/delete", new DeletePersonsHandler(new PersonDAOThread(dbService)));
        server.createContext("/update", new UpdatePersonsHandler(new PersonDAOThread(dbService)));

        server.setExecutor(Executors.newSingleThreadExecutor());

        server.start();
        System.out.println("Simple server started...");
    }
}
