package sbp.jdbc.web;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;
import sbp.jdbc.PersonDAOThread;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


/**
 * класс служит для обработки запроса POST /delete
 */
public class DeletePersonsHandler implements HttpHandler {
    private final PersonDAOThread dao;
    /**
     * Внедряет внешнюю зависимость PersonDAO
     */
    public DeletePersonsHandler(PersonDAOThread dao) {
        this.dao = dao;
    }

    /**
     * Обработчик, который вызывается для обработки HTTP Exchange
     * Читает POST запрос от клиента и принимает данные в формате JSON
     *
     * Бросает IOException в случае ошибки
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        OutputStream outputStream = null;

        try {
            outputStream = exchange.getResponseBody();

            StringBuilder htmlBuilder = new StringBuilder();

            if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
                String jsonPerson =
                        new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                                .lines()
                                .collect(Collectors.joining());

                JSONObject jsonObject = new JSONObject(jsonPerson);

                int agePerson = jsonObject.getInt("age");

                htmlBuilder.append("<html>")
                        .append("<body>")
                        .append("<h1>")
                        .append("Delete in DB all persons with age = " + agePerson + " : " + postDeletePersonsMethod(agePerson))
                        .append("</h1>")
                        .append("</body>")
                        .append("</html>");
            }

            String htmlStr = htmlBuilder.toString();
            exchange.sendResponseHeaders(200, htmlStr.length());

            outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        }finally {
         if (outputStream != null)
             outputStream.close();
        }
    }

    /**
     * метод удаляет объекты Person в БД при условии совпадении age
     *
     * возвращает строковое сообщение SUCCESS - в случае удачного завершения
     * и false - в случае Exception
     *
     */
    private String postDeletePersonsMethod(int age) {
        try {
            this.dao.deletePersonsByAge(age);
            return "SUCCESS";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";
        }
    }
}