package sbp.ArrayIterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * Класс ArrayIterator<T> реализует методы next() и hasNext() интерфейса Iterator<T>
 * для двумерных массивов
 */
public class ArrayIterator<T> implements Iterator<T>{
    /** двумерный массив, который передали в качестве параметра */
    private T[][] array;
    /** переменная , хранящее значение текущего столбца массива */
    private int currentI;
    /** переменная , хранящее значение текущего строки массива */
    private int currentJ;
    /** переменная , хранящее текущее значение, выбранное из массива */
    private T currentArrayElement;

    /**
     * Конструктор класса получает на вход двумерный массив
     * бросает исключение IllegalArgumentException, если входной массив некорректен
     * инициализирует переменный класса
     */
    public ArrayIterator(T[][] array){
        if(array == null || array.length == 0 || array[0].length == 0){
            throw new IllegalArgumentException();
        }
        this.array =  array;
        this.currentI = 0;
        this.currentJ = 0;
        this.currentArrayElement = null;
    }


    /**
     * Метод возвращает true, если еще не достигнут конец массива
     * и false - достигнут конец массива
     */
    @Override
    public boolean hasNext() {
        if( this.currentJ == this.array.length ){
            return false;
        }
        return true;
    }

    /**
     * Метод возвращает текущее значение массива currentArrayElement
     * Если достигнут конец массива и вызывается метод next - бросает NoSuchElementException
     */
    @Override
    public T next() {
        if(!hasNext())
            throw new NoSuchElementException();

        this.currentArrayElement = this.array[this.currentJ][this.currentI++];

        if( this.currentI  == this.array[this.currentJ].length ) {
            this.currentI = 0;
            ++this.currentJ;
        }
        return this.currentArrayElement;
    }

    /**
     * Метод remove - не поддерживается,
     * бросает исключение при вызове
     */
    @Override
    public void remove(){
        throw new UnsupportedOperationException();
    }

}
