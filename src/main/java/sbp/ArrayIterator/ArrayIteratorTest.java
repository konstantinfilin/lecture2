package sbp.ArrayIterator;


import org.junit.jupiter.api.Test;
import sbp.person.Person;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ArrayIteratorTest {
    Integer[][] arIntegerEmpty = {};
    Integer[][] arInteger0 = {{}};
    Integer[][] arInteger1 = {{9}};
    Integer[][] arInteger = {
            {1,10,2,11,15},
            {23,14,21,101,150},
            {13,18,19,3,16}
    };

    String[][] arString = {
            {"aaa"},
            {"bbb"}
    };

    Person[][] arPerson = {
            {
                    new Person("Phill", "Omsk", 20),
                    new Person("Николай", "Тоmsk", 29),
                    new Person("Андрей", "Москва", 23)
            }
    };

    /**
     * Проверка метода hasNext - возвращает true
     *
     * Создаем итератор с входным массивом целых чисел
     * метод возвращает true
     */
    @Test
    void hasNext_trueInt_test() {
        ArrayIterator<Integer> arrayIterator = new ArrayIterator<>(arInteger);
        assertTrue(arrayIterator.hasNext());
    }

    /**
     * Проверка метода hasNext - возвращает true
     *
     * Создаем итератор с входным массивом сторок
     * метод возвращает true
     */
    @Test
    void hasNext_trueStr_test() {
        ArrayIterator<String> arrayIterator = new ArrayIterator<>(arString);
        assertTrue(arrayIterator.hasNext());
    }

    /**
     * Проверка метода hasNext - возвращает true
     *
     * Создаем итератор с входным массивом Person
     * метод возвращает true
     */
    @Test
    void hasNext_truePerson_test() {
        ArrayIterator<Person> arrayIterator = new ArrayIterator<>(arPerson);
        assertTrue(arrayIterator.hasNext());
    }

    /**
     * Проверка метода hasNext - возвращает false
     *
     * Создаем итератор с входным пустым массивом Integer
     * метод возвращает IllegalArgumentException
     */
    @Test
    void hasNext_ExceptionEmpty_test() {
        assertThrows(IllegalArgumentException.class, ()->new ArrayIterator<>(arIntegerEmpty).hasNext());
    }

    /**
     * Проверка метода hasNext - возвращает false
     *
     * Создаем итератор с входным массивом Integer, ссылка на который равна null
     * метод возвращает IllegalArgumentException
     */
    @Test
    void hasNext_ExceptionNull_test() {
        assertThrows(IllegalArgumentException.class, ()->new ArrayIterator<>(null).hasNext());
    }

    /**
     * Проверка метода hasNext - бросает IllegalArgumentException
     *
     * Создаем итератор с входным массивом Integer из одного элемента
     * метод возвращает true
     */
    @Test
    void hasNext_ExceptionElementArray_test() {
        assertThrows(IllegalArgumentException.class, ()->new ArrayIterator<>(arInteger0).hasNext());
    }


    /**
     * Проверка работы метода next - бросает исключение NoSuchElementException
     *
     * В качестве входного параметра передаем массив с одим значениие - 9;
     * выполняем метод next() два раза - первый вовращает значение 9,
     * при втором выполение -  бросает NoSuchElementException
     *
     */
    @Test
    void next_NoSuchElementException_test() {
        ArrayIterator<Integer> arrayIterator = new ArrayIterator<>(arInteger1);
        assertEquals(arrayIterator.next(),9);
        assertThrows(NoSuchElementException.class, ()->arrayIterator.next());
    }

    /**
     * Проверка работы метода next - возвращает true
     *
     * В качестве входного параметра передаем двумерный массив String  {"aaa"},{"bbb"};
     * выполняем метод next() - при первом - вовращает значение "aaa",
     * при втором выполение - вовращает значение "bbb"
     */
    @Test
    void next_true_test() {
        ArrayIterator<String> arrayIterator = new ArrayIterator<>(arString);
        assertEquals(arrayIterator.next(),"aaa");
        assertEquals(arrayIterator.next(),"bbb");
    }

    /**
     * Проверка метода remove - бросает UnsupportedOperationException
     *
     * при вызове метода - бросается UnsupportedOperationException
     */
    @Test
    void remove_UnsupportedOperationException_test() {
        ArrayIterator<String> arrayIterator = new ArrayIterator<>(arString);
        assertThrows(UnsupportedOperationException.class, ()->arrayIterator.remove());
    }

}