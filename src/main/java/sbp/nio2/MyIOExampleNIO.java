package sbp.nio2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class MyIOExampleNIO
{
    /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) {
        if(fileName==null || fileName.isEmpty())
            return false;
        Path path = Paths.get(fileName);
        try {
            if (Files.exists(path)) {
                System.out.println("Абсолютный путь : " + path.toAbsolutePath());
                System.out.println("Родительский путь : " + path.getParent());
                if (Files.isRegularFile(path)) {
                    System.out.println("Размер : " + Files.size(path));
                    System.out.println("Время последнего изменения : " + Files.getLastModifiedTime(path));
                } else if (Files.isDirectory(path)) {
                }
                return true;
            }
        }catch (IOException ioException){
            System.out.println("workWithFile method throw " + ioException);
            ioException.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName){
        if( sourceFileName == null || destinationFileName == null ||
                sourceFileName.isEmpty() || destinationFileName.isEmpty() )
            return false;

        Path pathSourceFile = Paths.get(sourceFileName);
        Path pathDestinationFile = Paths.get(destinationFileName);

        if ( !Files.exists( pathSourceFile ) || !Files.isRegularFile( pathSourceFile ) ){
            return false;
        }

        try{
            Path destinationFile = Files.copy(pathSourceFile, pathDestinationFile, REPLACE_EXISTING);
            return Files.exists(destinationFile);

        }catch (IOException ioException){
            System.out.println("copyFile method throw " + ioException);
            ioException.printStackTrace();
            return false;
        }
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName){
        if( sourceFileName == null || destinationFileName == null ||
                sourceFileName.isEmpty() || destinationFileName.isEmpty() )
            return false;

        Path pathSourceFile = Paths.get(sourceFileName);
        Path pathDestinationFile = Paths.get(destinationFileName);

        if ( !Files.exists( pathSourceFile ) || !Files.isRegularFile( pathSourceFile ) ){
            return false;
        }

        try (
                InputStream in = Files.newInputStream(pathSourceFile);
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                OutputStream out = Files.newOutputStream(pathDestinationFile);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out))
        ) {
            int curInputByte;
            while ( ( curInputByte = reader.read() ) != -1 )
            {
                writer.write(curInputByte);
            }
        }catch (IOException ioException){
            System.out.println("copyBufferedFile method throw " + ioException);
            ioException.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName){
        if( sourceFileName == null || destinationFileName == null ||
                sourceFileName.isEmpty() || destinationFileName.isEmpty() )
            return false;

        Path pathSourceFile = Paths.get(sourceFileName);
        Path pathDestinationFile = Paths.get(destinationFileName);

        if ( !Files.exists( pathSourceFile ) || !Files.isRegularFile( pathSourceFile ) ){
            return false;
        }

        try (
                BufferedReader reader = Files.newBufferedReader(pathSourceFile);
                BufferedWriter writer = Files.newBufferedWriter(pathDestinationFile)
        ){
            String currentLine;
            while((currentLine = reader.readLine()) != null) {
                writer.write(currentLine);
            }
        } catch (IOException ioException) {
            System.out.println("copyFileWithReaderAndWriter method throw " + ioException);
            ioException.printStackTrace();
            return false;
        }
        return true;
    }
}
