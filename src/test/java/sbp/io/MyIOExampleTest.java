package sbp.io;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MyIOExampleTest extends MyIOExample {

    /**
     * проверяем работу workWithFile с несуществующей сущностью
     *
     * В качестве входного параметра для тестируемого метода передаем путую строку
     */
    @Test
    void emptyFileName_WorkWithFile_test() {
        assertEquals(false, workWithFile(""));
    }

    /**
     * проверяем работу workWithFile с существующим файлом
     *
     * В качестве входного параметра для тестируемого метода передаем имя существующего файла
     */
    @Test
    void realFileName_WorkWithFile_test() {
        assertEquals(true, workWithFile("readme.md"));
    }

    /**
     * проверяем работу workWithFile с несуществующим файлом
     *
     * В качестве входного параметра для тестируемого метода передаем имя несуществующего файла
     */
    @Test
    void unrealFileName_WorkWithFile_test() {
        assertEquals(false, workWithFile("readme.md1"));
    }

    /**
     * проверяем работу workWithFile с существующей директорией
     *
     * В качестве входного параметра для тестируемого метода передаем имя существующей директории
     */
    @Test
    void realDirectoryName_WorkWithFile_test() {
        assertEquals(true, workWithFile("target"));
    }

    /**
     * проверяем работу workWithFile с несуществующей директорией
     *
     * В качестве входного параметра для тестируемого метода передаем имя несуществующей директории
     */
    @Test
    void unrealDirectoryName_WorkWithFile_test() {
        assertEquals(false, workWithFile("targets"));
    }


    /**
     * проверяем работу метода копирования файлов copyFile
     *
     * в качестве входного параметр для файла источника указываем сущестыующий файл,
     * в качестве входного параметра для файла результата передаем имя создаваемой копии
     * в случае успешного копирования метод должен вернуть true
     */
    @Test
    void copyFile_test() throws Exception {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(true, myIOExample.copyFile("readme.md", "readme_copy.md"));
    }


    /**
     * Бросаем exception в методе копирования файлов copyFile
     *
     * в качестве входного параметр для файла источника указываем несущестыующий файл
     */
    @Test
    void copyFile_unrealFileSource_Exception_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertThrows(Exception.class, ()-> myIOExample.copyFile("readme.md1", "readme_copy.md"));
    }

    /**
     * Бросаем exception в методе копирования файлов copyFile
     *
     * в качестве входного параметра для файла результата указываем несущестыующий файл
     */
    @Test
    void copyFile_unrealFileDes_Exception_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertThrows(Exception.class, ()-> myIOExample.copyFile("readme.md", ""));
    }

    /**
     * Бросаем exception в методе копирования файлов copyFile
     *
     * в качестве входных параметров указываем несущестыующие файлы
     */
    @Test
    void copyFile_unrealFileSourceDes_Exception_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertThrows(Exception.class, ()-> myIOExample.copyFile("", ""));
    }


    /**
     * проверяем работу метода копирования файлов CopyBufferedFile
     *
     * в качестве входного параметр для файла источника указываем сущестыующий файл,
     * в качестве входного параметра для файла результата передаем имя создаваемой копии
     * в случае успешного копирования метод должен вернуть true
     */
    @Test
    void copyBufferedFile_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(true, myIOExample.copyBufferedFile("readme.md", "readme_copy.md"));
    }

    /**
     * проверяем работу метода копирования файлов CopyBufferedFile
     *
     * в качестве входного параметр для файла источника указываем несущестыующий файл,
     * метод должен вернуть false
     */
    @Test
    void copyBufferedFile_unrealFileSource_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(false, myIOExample.copyBufferedFile("readme.md1", "readme_copy.md"));
    }

    /**
     * проверяем работу метода копирования файлов CopyBufferedFile
     *
     * в качестве входного параметр для файла результата указываем несущестыующий файл,
     * метод должен вернуть false
     */
    @Test
    void copyBufferedFile_unrealFileDes_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(false, myIOExample.copyBufferedFile("readme.md", ""));
    }

    /**
     * проверяем работу метода копирования файлов CopyBufferedFile
     *
     * в качестве входных параметров для файла источника и файла результата указываем несущестыующие файлы,
     * метод должен вернуть false
     */
    @Test
    void copyBufferedFile_unrealFileSourceAndDes_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(false, myIOExample.copyBufferedFile("readme.md1", ""));
    }



    /**
     * проверяем работу метода копирования файлов copyFileWithReaderAndWriter
     *
     * в качестве входного параметр для файла источника указываем сущестыующий файл,
     * в качестве входного параметра для файла результата передаем имя создаваемой копии
     * в случае успешного копирования метод должен вернуть true
     */
    @Test
    void copyFileWithReaderAndWriter_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(true, myIOExample.copyBufferedFile("readme.md", "readme_copy.md"));
    }

    /**
     * проверяем работу метода копирования файлов copyFileWithReaderAndWriter
     *
     * в качестве входного параметр для файла источника указываем несущестыующий файл,
     * метод должен вернуть false
     */
    @Test
    void copyFileWithReaderAndWriter_unrealFileResource_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(false, myIOExample.copyBufferedFile("readme.md1", "readme_copy.md"));
    }


    /**
     * проверяем работу метода копирования файлов copyFileWithReaderAndWriter
     *
     * в качестве входного параметр для файла результата указываем несущестыующий файл,
     * метод должен вернуть false
     */
    @Test
    void copyFileWithReaderAndWriter_unrealFileDes_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(false, myIOExample.copyBufferedFile("readme.md", ""));
    }

    /**
     * проверяем работу метода копирования файлов copyFileWithReaderAndWriter
     *
     * в качестве входных параметров указываем несущестыующие файлы,
     * метод должен вернуть false
     */
    @Test
    void copyFileWithReaderAndWriter_unrealFileResourceAndDes_test() {
        MyIOExample myIOExample = new MyIOExample();
        assertEquals(false, myIOExample.copyBufferedFile("readme.md1", ""));
    }
}