package sbp.collections;

import org.junit.jupiter.api.Test;
import sbp.person.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SortPersonByNameComparatorTest {

    /**
     * Проверка метода - имя person1 < имени person2 в лексикографическом порядке
     *
     * Сравниваем две персоны с именами Антон и Денис
     */
    @Test
    void Less0_compare_test() {
        Person person1 = new Person("Антон", "Москва", 30);
        Person person2 = new Person("Денис", "Омск", 33);
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        assertTrue( sortPersonByNameComparator.compare(person1,person2) < 0);

    }

    /**
     * Проверка метода - имя person1 > имени person2 в лексикографическом порядке
     *
     * Сравниваем две персоны с именами Денис и Антон
     */
    @Test
    void more0_compare_test() {
        Person person1 = new Person("Антон", "Москва", 30);
        Person person2 = new Person("Денис", "Омск", 33);
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        assertTrue( sortPersonByNameComparator.compare(person2,person1) > 0);

    }

    /**
     * Проверка метода - имя person1 == имени person2 в лексикографическом порядке
     *
     * Сравниваем две персоны с именами Денис и Денис
     */
    @Test
    void equal_compare_test() {
        Person person1 = new Person("Денис", "Омск", 33);
        Person person2 = new Person("Денис", "Омск", 33);
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        assertTrue( sortPersonByNameComparator.compare(person2,person1) == 0);

    }

    /**
     * Проверка метода - сортировка массива по имени в лексикографическом порядке
     *
     * Сравниваем две персоны с именами Денис и Антон
     */
    @Test
    void array_compare_test(){
        Person[] personArr = new Person[]{
                new Person("Денис", "Омск", 33),
                new Person("Антон", "Москва", 30)
        };
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        Arrays.sort(personArr, sortPersonByNameComparator);
        assertTrue(personArr[0].getName().equalsIgnoreCase("Антон"));
        assertTrue(personArr[1].getName().equalsIgnoreCase("Денис"));
    }

    /**
     * Проверка метода - сортировка коллекции по имени в лексикографическом порядке
     *
     * Сравниваем две персоны с именами Денис и Антон
     */
    @Test
    void collection_compare_test(){
        Person[] personArr = new Person[]{
                new Person("Денис", "Омск", 33),
                new Person("Антон", "Москва", 30)
        };
        List<Person> personList = new ArrayList<>( Arrays.asList(personArr) );
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        Collections.sort(personList, sortPersonByNameComparator);
        assertTrue(personList.get(0).getName().equalsIgnoreCase("Антон"));
        assertTrue(personList.get(1).getName().equalsIgnoreCase("Денис"));
    }

    /**
     * метод бросает исключение IllegalArgumentException
     *
     * В качестве первого параметра передаем null, первым параметром person
     */
    @Test
    void throwsIllegalArgumentExceptionFirstParam_compare_test() {
        Person person = new Person("Денис", "Омск", 33);
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        assertThrows(IllegalArgumentException.class, ()-> sortPersonByNameComparator.compare( null,person ));
    }

    /**
     * метод бросает исключение IllegalArgumentException
     *
     * В качестве первого параметра передаем person , первым параметром null
     */
    @Test
    void throwsIllegalArgumentExceptionSecondParam_compare_test() {
        Person person = new Person("Денис", "Омск", 33);
        SortPersonByNameComparator sortPersonByNameComparator = new SortPersonByNameComparator();
        assertThrows(IllegalArgumentException.class, ()-> sortPersonByNameComparator.compare( person, null ));
    }
}