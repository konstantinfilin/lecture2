package sbp.collections;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CustomDigitComparatorTest {

    /**
     * Первое из пары целых чисел входного параметра - четное
     *
     * В качестве параметра integer1 передаем 4, integer2 - 3
     */
    @Test
    void integer1EqualEvenNumber4_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        assertEquals(-1,customDigitComparator.compare(4,3));
    }

    /**
     * Первое из пары целых чисел входного параметра - нечетное
     *
     * В качестве параметра integer1 передаем 3, integer2 - 4
     */
    @Test
    void integer1EqualOddNumber3_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        assertEquals(1,customDigitComparator.compare(3,4));
    }

    /**
     * Оба входных параметра  - четные и одинаковые
     *
     * В качестве параметра integer1 передаем 4, integer2 - 4
     */
    @Test
    void bothNumber4_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        assertEquals(0,customDigitComparator.compare(4,4));
    }

    /**
     * Оба входных параметра  - нечетные и одинаковые
     *
     * В качестве параметра integer1 передаем 3, integer2 - 3
     */
    @Test
    void bothNumber3_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        assertEquals(0,customDigitComparator.compare(3,3));
    }

    /**
     * метод бросает исключение IllegalArgumentException
     *
     * В качестве первого параметра integer1 передаем null, integer2 - 3
     */
    @Test
    void throwsIllegalArgumentExceptionInteger1_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        assertThrows(IllegalArgumentException.class, ()-> customDigitComparator.compare(null,3));
    }

    /**
     * метод бросает исключение IllegalArgumentException
     *
     * В качестве второго параметра integer2 передаем null, integer1 - 3
     */
    @Test
    void throwsIllegalArgumentExceptionInteger2_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        assertThrows(IllegalArgumentException.class, ()-> customDigitComparator.compare(3, null));
    }

    /**
     * упорядочение массива из четных и нечетых целых чисел
     *
     * В качестве компаратора в Arrays.sort используем customDigitComparator
     */
    @Test
    void array_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        Integer[] input = new Integer[]{ 3, 7, 10, 20};
        Integer[] outTrue = new Integer[]{ 20, 10, 3, 7};
        Arrays.sort(input,customDigitComparator);
        assertArrayEquals(outTrue,input);
    }

    /**
     * упорядочение коллекции из четных и нечетых целых чисел
     *
     * В качестве компаратора в Collections.sort используем customDigitComparator
     */
    @Test
    void collection_compare_test() {
        CustomDigitComparator customDigitComparator = new CustomDigitComparator();
        Integer[] input = new Integer[]{ 3, 7, 10, 20};
        Integer[] outTrue = new Integer[]{ 20, 10, 3, 7};
        List<Integer> integerInputList = new ArrayList<>( Arrays.asList(input) );
        Collections.sort(integerInputList,customDigitComparator);
        List<Integer> integerOutputList = new ArrayList<>( Arrays.asList(outTrue) );
        assertEquals(true, integerInputList.containsAll( integerOutputList ));
    }
}