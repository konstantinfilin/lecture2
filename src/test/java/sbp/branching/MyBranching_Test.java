package sbp.branching;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranching_Test {

    /**
     * Проверка метода для нахождения максимального значения из дву чисел i1 и i2
     *
     * Создается mock на объект Utils, задаем различные значениях для заглушек
     * Utils#utilFunc1() и Utils#utilFunc2(), сравниваем результат работы метода
     * с заданными значениями i1, i2, 0.
     *
     */

    @Test
    public void maxIntTest(){
        final int i1=5;
        final int i2=2;
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching  = new MyBranching(utilsMock);

        Mockito.when(utilsMock.utilFunc1("Hello")).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Assert.assertEquals(0,myBranching.maxInt(i1, i2));

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Assert.assertEquals(0,myBranching.maxInt(i1, i2));

        Mockito.when(utilsMock.utilFunc1("Hello")).thenReturn(true);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Assert.assertEquals(i1,myBranching.maxInt(i1, i2));

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Assert.assertEquals(i1,myBranching.maxInt(i1, i2));
    }

    /**
     * Метод представляет unit-тест для проверки метода ifElseExample.
     * создается mock на объект и проверяется результаты выполнения метода в зависимости от
     * от различных значений возвращаемых заглушками на Utils#utilFunc2() и Utils#utilFunc1()
     * метод должен возвращает true, если Utils#utilFunc2() возвращает true
     * вне зависимости от значений возвращаемых Utils#utilFunc1()
     */

    @Test
    public void ifElseExampleTest(){

        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching  = new MyBranching(utilsMock);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1("Hello")).thenReturn(true);
        Assert.assertTrue(myBranching.ifElseExample());

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Assert.assertFalse(myBranching.ifElseExample());

        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(2)).utilFunc2();
    }

    /**
     * Метод представляет unit-тест для проверки метода switchExample в зависимости от входного
     * параметра.
     * создается mock на объект и проверяется выполнения методов Utils#utilFunc1 и Utils#utilFunc2
     * при входящем i = 0
     */

    @Test
    public void switchExampleTest_0(){
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching  = new MyBranching(utilsMock);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        myBranching.switchExample(0);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1("abc2");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
     * Метод представляет unit-тест для проверки метода switchExample() в зависимости от входного
     * параметра.
     * создается mock на объект и проверяется выполнения методов Utils#utilFunc1 и Utils#utilFunc2
     * при входящем i = 1
     */

    @Test
    public void switchExampleTest_1(){
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching  = new MyBranching(utilsMock);
        myBranching.switchExample(1);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1("abc");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
     * Метод представляет unit-тест для проверки метода switchExample в зависимости от входного
     * параметра.
     * создается mock на объект и проверяется выполнения методов Utils#utilFunc1 и Utils#utilFunc2
     * при входящем i = 2
     */
    @Test
    public void switchExampleTest_2(){
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching  = new MyBranching(utilsMock);
        myBranching.switchExample(2);

        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1("abc");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }
}