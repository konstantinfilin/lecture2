package sbp.jdbc;

import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import static org.junit.jupiter.api.Assertions.*;

class DBServiceTest {

    /**
     * Проверка executeQuery - возврат false
     *
     * Во входном параметре -строку запроса - передаем null
     * метод возвращает false
     *
     */
    @Test
    void executeQuery_falseNull_test() throws SQLException {
     DBService dbService =  new DBService();
        boolean result = dbService.executeQuery(null);
        assertFalse(result);
    }

    /**
     * Проверка executeQuery - возврат false
     *
     * Во входном параметре передаем пустую строку запроса
     * метод возвращает false
     *
     */
    @Test
    void executeQuery_falseEmpty_test() throws SQLException {
        DBService dbService =  new DBService();
        boolean result = dbService.executeQuery("");
        assertFalse(result);
    }


    /**
     * Проверка executeQuery - возврат true
     *
     * Во входном параметре передаем SQL Select строку запроса
     * метод возвращает true
     */
    @Test
    void executeQuery_trueSelect_test() throws SQLException {
        DBService dbService =  new DBService();
        boolean result = dbService.executeQuery("select * from persons");
        assertTrue(result);
    }

    /**
     * Проверка executeQuery -  возврат false
     *
     * SQL запрос к несуществующей таблице вызывает исключение SQLException
     * метод возвращает false
     */
    @Test
    void executeQuery_SQL_test() throws SQLException {
        DBService dbService =  new DBService();
        boolean result = dbService.executeQuery("select * from persons_1");
        assertFalse(result);
    }

    /**
     * Проверка метода executeUpdate - бросает исключение IllegalArgumentException
     *
     * во входном параметре передаем str == null
     * Метод бросает IllegalArgumentException
     */
    @Test
    void executeUpdate_FailInputParamNull_test()  {
        DBService dbService =  new DBService();
        assertThrows(IllegalArgumentException.class, ()->dbService.executeUpdate(null));
    }

    /**
     * Проверка метода executeUpdate - бросает исключение IllegalArgumentException
     *
     * во входном параметре передаем пустую str
     * Метод бросает IllegalArgumentException
     */
    @Test
    void executeUpdate_FailInputParaIsEmpty_test() {
        DBService dbService =  new DBService();
        assertThrows(IllegalArgumentException.class, ()->dbService.executeUpdate(""));
    }

    /**
     * Проверка метода executeUpdate - возвращает -1
     *
     * во входном параметре передаем SQL запрос с несуществующей таблицей
     * Метод вовращает -1
     */
    @Test
    void executeUpdate_NegativeValue_test()  {
        DBService dbService =  new DBService();
        int result = dbService.executeUpdate("delete from persons_1");
        assertEquals(result, -1);
    }

    /**
     * Проверка метода executeUpdate - возвращает кол-во обработанных person
     *
     * во входном параметре передаем SQL запрос на удаление person , у которых id > 10
     * Метод вовращает 4
     */
    @Test
    void executeUpdate_PositiveValue_test()  {
        DBService dbService =  new DBService();
        int result = dbService.executeUpdate("delete from persons where id > 10");
        assertEquals(result, 4);
    }
}