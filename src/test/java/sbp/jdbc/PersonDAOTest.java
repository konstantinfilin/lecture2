package sbp.jdbc;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.person.Person;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonDAOTest {

    /**
     * Проверяем работу метода createTable - возвращает true
     * создаем новую таблицу,
     * при успешом завершении возвращает true
     */
    @Test
    void createTable_true_test() {
       PersonDAO personDAO = new PersonDAO( new DBService() );
       assertTrue(personDAO.createTable());
    }

    /**
     * Проверяем работу метода createTable - возврат false
     * создаем Mock объект, который вызывает исключение SQLException
     * метод возвращает false
     */
    @Test
    void createTable_false_test() throws SQLException {
        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.when(dbServiceMock.executeQuery(Mockito.anyString())).thenThrow(SQLException.class);
        PersonDAO personDAO = new PersonDAO( dbServiceMock );
        assertFalse(personDAO.createTable());
    }

    /**
     * Проверяем работу метода deleteTable - возврат true
     * удаляем существующую таблицу,
     * при успешом завершении возвращает true
     */
    @Test
    void deleteTable_true_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        assertTrue(personDAO.deleteTable());
    }

    /**
     * Проверяем работу метода deleteTable - возврат false
     * создаем Mock объект, который вызывает исключение SQLException
     * метод возвращает false
     */
    @Test
    void deleteTable_false_test() throws SQLException {
        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.when(dbServiceMock.executeQuery(Mockito.anyString())).thenThrow(SQLException.class);
        PersonDAO personDAO = new PersonDAO( dbServiceMock );
        assertFalse(personDAO.deleteTable());
    }

    /**
     * Проверка метода addPerson по добавлению person в DB - возвращает true.
     * Созлаем Person, вызываем метод для добавления Person
     * метод возвращает true
     */
    @Test
    void addPerson_true_test() {
        Person person = new Person("Nik","London", 30);
        PersonDAO personDAO = new PersonDAO( new DBService() );
        boolean result = personDAO.addPerson(person);
        assertTrue(result);
    }

    /**
     * Проверка метода addPerson по добавлению person в DB - возвращает false.
     * Созлаем Person, создаем Mock объект, который вызывает исключение SQLException
     * вызываем метод для добавления Person
     * метод возвращает false
     */
    @Test
    void addPerson_falseSQLException_test() throws SQLException {
        Person person = new Person("Nik","London", 30);
        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.when(dbServiceMock.executeQuery(Mockito.anyString())).thenThrow(SQLException.class);

        PersonDAO personDAO = new PersonDAO( dbServiceMock );
        boolean result = personDAO.addPerson(person);
        assertFalse(result);
    }

    /**
     * Проверка метода addPerson по добавлению person в DB - возвращает false.
     * Создаем Person, создаем Mock объект, который вызывает исключение SQLException
     * вызываем метод для добавления Person
     * метод возвращает false
     */
    @Test
    void addPerson_false_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        boolean result = personDAO.addPerson(null);
        assertFalse(result);
    }

    /**
     * Проверка метода getAllPersons - возвращает true
     *
     * Создаем тестовый список с одним значение, для простоты в БВ тоже только один person
     * с теми же значениеми
     * результат - true
     */
    @Test
    void getAllPersons_true_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        List<Person> listTest = Arrays.asList(new Person("НИКОЛАЙ", "ЛОНДОН", 32));
        List<Person> listDB = personDAO.getAllPersons();
        assertArrayEquals(listTest.toArray(), listDB.toArray());
    }

    /**
     * Проверка метода getAllPersons - возвращает false
     *
     * Создаем тестовый список с одним значение, для простоты в БВ тоже только один person
     * с значениемя , отличными от тестового
     * результат - false
     */
    @Test
    void getAllPersons_false_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        List<Person> listTest = Arrays.asList(new Person("НИКОЛАЙ", "ЛОНДОН", 42));
        List<Person> listDB = personDAO.getAllPersons();
        assertFalse(listTest.toArray().equals(listDB.toArray()));
    }

    /**
     * Проверка метода getAllPersons - возвращает прустой список
     *
     * SQL запроса возвращает пустой список, для этого удаляем все из DB
     * метод возвращает пустой список
     */
    @Test
    void getAllPersons_EmptyDB_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        List<Person> listDB = personDAO.getAllPersons();
        assertTrue(listDB.isEmpty());
    }

    /**
     * Проверка метода getAllPersons - возвращает null
     *
     * cоздаем Mock объект, который вызывает исключение SQLException
     * вызываем метод для получения списка Person
     *  метод возвращает null
     */
    @Test
    void getAllPersons_falseSQLEXception_test() throws SQLException {
        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.when(dbServiceMock.executeQuery(Mockito.anyString())).thenThrow(SQLException.class);

        PersonDAO personDAO = new PersonDAO( dbServiceMock );
        List<Person> result = personDAO.getAllPersons();
        assertEquals(result, null);
    }



    /**
     * Проверка метода getPersonsByName - возвращает список с именами person НИКОЛАЙ
     *
     * добавили в DB person с именем НИКОЛАЙ
     * SQL запрос возвращает список из DB, удовлетворяющий запросу
     * метод возвращает список с именами НИКОЛАЙ
     */
    @Test
    void getPersonsByName_ListNik_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        List<Person> listDB = personDAO.getPersonsByName("НИКОЛАЙ");
        for (Person person: listDB ) {
            assertTrue(person.getName().equals("НИКОЛАЙ"));
        }
    }


    /**
     * Проверка метода getPersonsByName - возвращает прустой список
     *
     * SQL запрос возвращает пустой список для этого запроса из DB, т.к. база не содержит
     * person с именем НИКОЛАЙ
     *
     * метод возвращает пустой список
     */
    @Test
    void getPersonsByName_EmptyList_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        List<Person> listDB = personDAO.getPersonsByName("НИКОЛАЙ");
        assertTrue(listDB.isEmpty());
    }

    /**
     * Проверка метода getPersonsByName - возвращает null
     *
     * cоздаем Mock объект, который вызывает исключение SQLException
     * вызываем метод для получения списка Person, удовлетворяющий запросу
     * метод возвращает null
     */
    @Test
    void getPersonsByName_NullSQLEXception_test() throws SQLException {
        DBService dbServiceMock = Mockito.mock(DBService.class);
        Mockito.when(dbServiceMock.executeQuery(Mockito.anyString())).thenThrow(SQLException.class);

        PersonDAO personDAO = new PersonDAO( dbServiceMock );
        List<Person> result = personDAO.getPersonsByName("НИКОЛАЙ");
        assertEquals(result, null);
    }


    /**
     * Проверка метода updateCityPerson - возвращает -1
     *
     * Метод значение -1 при неверных входных параметрах,
     * для этого передаем в качестве входного параметра пустую строку
     */
    @Test
    void updateCityPerson_NegativeResult_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.updateCityPerson("", 1);
        assertEquals(result, -1);
    }

    /**
     * Проверка метода updateCityPerson - возвращает -1
     *
     * Метод значение -1 при неверных входных параметрах,
     * для этого передаем в качестве входного параметра id < 0
     */
    @Test
    void updateCityPerson_NegativeResult2_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.updateCityPerson("New-York", -100);
        assertEquals(result, -1);
    }

    /**
     * Проверка метода updateCityPerson - возвращает -1
     *
     * Метод значение -1 при неверных входных параметрах,
     * для этого передаем в качестве входного параметра null
     */
    @Test
    void updateCityPerson_NegativeResult3_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.updateCityPerson(null, 1);
        assertEquals(result, -1);
    }

    /**
     * Проверка метода updateCityPerson - возвращает кол-во измененных записей в DB
     * при успешном выполение SQL запроса,
     *
     * для этого передаем в качестве входного параметра новый город и верный id
     */
    @Test
    void updateCityPerson_PositiveValue_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.updateCityPerson("New-York", 16);
        assertEquals(result, 1);
    }

    /**
     * Проверка метода updateCityPerson - возвращает 0 в качастве кол-ва измененных данных
     *
     * для этого передаем в качестве входного параметра новый город и несуществующий id
     */
    @Test
    void updateCityPerson_ZerroValue_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.updateCityPerson("New-York", 160);
        assertEquals(result, 0);
    }


    /**
     * Проверка метода deletePersonsByAge - возвращает -1
     *
     * Метод значение -1 при неверных входных параметрах,
     * для этого передаем в качестве входного параметра значение возраста < 0
     */
    @Test
    void deletePersonsByAge_NegativeResult_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.deletePersonsByAge(-10);
        assertEquals(result, -1);
    }

    /**
     * Проверка метода updateCityPerson - возвращает кол-во удаленных записей в DB
     * при успешном выполение SQL запроса,
     *
     * для этого передаем в качестве входного параметра возраст person = 42
     */
    @Test
    void deletePersonsByAge_PositiveValue_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.deletePersonsByAge(42);
        assertEquals(result, 1);
    }


    /**
     * Проверка метода deletePersonsByAge - возвращает 0 в качастве кол-ва удаленных данных
     *
     * для этого передаем в качестве входного параметра несуществующий в DB возраст
     */
    @Test
    void deletePersonsByAge_ZerroValue_test() {
        PersonDAO personDAO = new PersonDAO( new DBService() );
        int result = personDAO.deletePersonsByAge(160);
        assertEquals(result, 0);
    }
}