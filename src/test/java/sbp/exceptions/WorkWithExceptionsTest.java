package sbp.exceptions;

import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;

import static org.junit.jupiter.api.Assertions.*;

class WorkWithExceptionsTest extends MyIOExample {

    /**
     * проверка метода exceptionProcessing
     *
     * в ходе работы метода создается исключение,
     * проверяем обработку брошенного исключения кастоным классом обработки исключения
     */
    @Test
    void exceptionProcessing() {
        WorkWithExceptions work = new WorkWithExceptions();
        assertThrows(MyClassException.class, ()-> work.exceptionProcessing());
    }


}