package sbp.person;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    /**
     * метод возвращает false
     *
     * в качестве параметра передается null
     */
    @Test
    void argumentIsNull_equals_test() {
        Person person = new Person("Денис", "Омск", 33);
        assertEquals( false, person.equals(null) );
    }

    /**
     * метод возвращает true
     *
     * В качестве входного параметра передаем один и тот же объект Person
     */
    @Test
    void argumentIsPerson_equals_test() {
        Person person = new Person("Денис", "Омск", 33);
        assertEquals( true, person.equals( person ) );
    }

    /**
     * метод возвращает false
     *
     * сравниваем два разных объекта Person
     */
    @Test
    void compareDifferentPerson_equals_test() {
        Person person = new Person("Денис", "Омск", 33);
        Person second = new Person("Андрей", "Омск", 35);
        assertEquals( false, person.equals( second ) );
    }

    /**
     * метод возвращает false
     *
     * В качестве входного параметра передаем объект, не являющийся объектом Person
     */
    @Test
    void argumentIsNotPerson_equals_test() {
        Person person = new Person("Денис", "Омск", 33);
        Integer intParam = 10;
        assertEquals( false, person.equals( intParam ) );
    }

    /**
     * метод бросает исключение IllegalArgumentException
     *
     * в качестве параметра передается null
     */
    @Test
    void throwIllegalArgumentException_compareTo_test() {
            Person person = new Person("Денис", "Омск", 33);
            assertThrows(IllegalArgumentException.class, ()-> person.compareTo(null) );
    }

    /**
     * Метод сортирует объекты Person сначала по названию города, а затем по имени в лексикографическом порядке
     *
     * Создаем массив Person, сравниваем элементы из входного массива с конкретными значениями из отсортированого
     * массива
     */
    @Test
    void sortByCityByName_compareTo_test() {
        Person firstMatch = new Person("Антон", "Москва", 30);
        Person secondMatch = new Person("Андрей", "Омск", 35);
        Person thirdMath = new Person("Борис", "Тверь", 18);

        Person[] personArray = new Person[]{
                secondMatch,
                new Person("Денис", "Омск", 33),
                new Person("Влад", "Москва", 26),
                new Person("Леонид", "Омск", 31),
                thirdMath,
                new Person("Николай", "Москва", 36),
                new Person("Михаил", "Тверь", 43),
                firstMatch,
                new Person("Ирина", "Тверь", 34)
        };
        Arrays.sort(personArray);
        assertEquals( personArray[0], firstMatch );
        assertEquals( personArray[3], secondMatch );
        assertEquals( personArray[6], thirdMath );
    }
}