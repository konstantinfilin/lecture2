package sbp.stream;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BracketsVerifyTest {

    /**
     * проверка метода bracketsVerification() на равенство false
     * входная сторка с пустым кол-вом элементов данных
     */
    @Test
    void emptyInput_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(false, bv.bracketsVerification(""));
    }

    /**
     * проверка метода bracketsVerification() на равенство false
     * входная сторка null
     */
    @Test
    void nullInput_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(false, bv.bracketsVerification(null));
    }

    /**
     * проверка метода bracketsVerification() на равенство false
     * входная сторка "[(])"
     */
    @Test
    void uncorrectInput1_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(false, bv.bracketsVerification("[(])"));
    }


    /**
     * проверка метода bracketsVerification() на равенство false
     * входная сторка "{"
     */
    @Test
    void uncorrectInput2_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(false, bv.bracketsVerification("{"));
    }

    /**
     * проверка метода bracketsVerification() на равенство false
     * входная сторка ")"
     */
    @Test
    void uncorrectInput3_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(false, bv.bracketsVerification(")"));
    }

    /**
     * проверка метода bracketsVerification() на равенство false
     * входная сторка "({[][({((()))})(])]})"
     */
    @Test
    void uncorrectInput4_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(false, bv.bracketsVerification("({[][({((()))})(])]})"));
    }

    /**
     * проверка метода bracketsVerification() на равенство true
     * входная сторка "(){}[]"
     */
    @Test
    void correctInput1_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(true, bv.bracketsVerification("(){}[]"));
    }

    /**
     * проверка метода bracketsVerification() на равенство true
     * входная строка "()"
     */
    @Test
    void correctInput2_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(true, bv.bracketsVerification("()"));
    }

    /**
     * проверка метода bracketsVerification() на равенство true
     * входная строка "{}"
     */
    @Test
    void correctInput3_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(true, bv.bracketsVerification("{}"));
    }

    /**
     * проверка метода bracketsVerification() на равенство true
     * входная строка "[]"
     */
    @Test
    void correctInput4_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(true, bv.bracketsVerification("[]"));
    }

    /**
     * проверка метода bracketsVerification() на равенство true
     * входная строка "{([({})])}"
     */
    @Test
    void correctInput5_bracketsVerification_test() {
        BracketsVerify bv = new BracketsVerify();
        assertEquals(true, bv.bracketsVerification("{([({})])}"));
    }


}