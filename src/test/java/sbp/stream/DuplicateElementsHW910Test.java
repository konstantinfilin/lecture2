package sbp.stream;

import org.junit.jupiter.api.Test;
import sbp.user.User;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateElementsHW910Test {

    List<Integer> integerList = new ArrayList<>(Arrays.asList(2, 5, 1, 3, 5, 6, 7, 8, 9,1,6));
    List<String> stringList = new ArrayList<>(
            Arrays.asList("Moscow", "New York", "London", "Paris", "Berlin", "London", "Berlin"));

    List<User> userList = Arrays.asList(
            new User("Victor", "Smirnov",  48, 157, 91, "Tomsk"),
            new User("Denis", "Petrov",  18, 118, 100, "Omsk"),
            new User("Anton", "Sverdlov",  38, 167, 90, "Omsk"),
            new User("Victor", "Smirnov",  48, 157, 91, "Tomsk"),
            new User("Vlad", "Ivanov",  70, 160, 90, "Moscow")
    );

    Queue<String> stringQueue = new ArrayDeque<>(
            Arrays.asList("Алматы", "Киев", "Минск", "Львов", "Дубна", "Киев", "Ростов", "Алматы")
    );
    Queue<User> userQueue = new ArrayDeque<>(
            Arrays.asList(
                    new User("Victor", "Smirnov",  48, 157, 91, "Tomsk"),
                    new User("Denis", "Petrov",  18, 118, 100, "Omsk"),
                    new User("Anton", "Sverdlov",  38, 167, 90, "Omsk"),
                    new User("Victor", "Smirnov",  48, 157, 91, "Tomsk"),
                    new User("Vlad", "Ivanov",  70, 160, 90, "Moscow")
            )
    );

    /**
     * проверка метода duplicateSet на список с элементами [1, 5, 6]
     * создаем список с элементвами [1, 5, 6] и сравниваем его с результоь работы метода
     * при входном значении списка List<Integer> [2, 5, 1, 3, 5, 6, 7, 8, 9, 1, 6]
     */
    @Test
    void listInteger_duplicateSet_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<Integer> trueAnswerArray = Arrays.asList(1, 5, 6);
        assertEquals(trueAnswerArray,hw.duplicateSet(integerList));
    }


    /**
     * проверка метода duplicateSet на равенство [Berlin, London]
     * создаем список с элементвами [Berlin, London] и сравниваем его с результоь работы метода
     * при входном значении списка List<String> ["Moscow", "New York", "London", "Paris", "Berlin", "London", "Berlin"]
     */
    @Test
    void listString_duplicateSet_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<String> trueAnswerArray = Arrays.asList("Berlin", "London");
        assertEquals(trueAnswerArray,hw.duplicateSet(stringList));
    }

    /**
     * проверка метода duplicateSet на равенство [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * создаем список с [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * и сравниваем его с результоь работы метода при входном значении списка List<User>
     */
    @Test
    void listUser_duplicateSet_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<User> trueAnswerArray = Arrays.asList(
                new User("Victor", "Smirnov",  48, 157, 91, "Tomsk")
        );
        assertEquals(trueAnswerArray,hw.duplicateSet(userList));
    }

    /**
     * проверка метода duplicateSetStream на список с элементами [1, 5, 6]
     * создаем список с элементвами [1, 5, 6] и сравниваем его с результоь работы метода
     * при входном значении списка List<Integer> [2, 5, 1, 3, 5, 6, 7, 8, 9, 1, 6]
     */
    @Test
    void listInteger_duplicateSetStream_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<Integer> trueAnswerArray = Arrays.asList(1, 5, 6);
        assertEquals(trueAnswerArray,hw.duplicateSetStream(integerList));
    }

    /**
     * проверка метода duplicateSetStream на равенство [Berlin, London]
     * создаем список с элементвами [Berlin, London] и сравниваем его с результоь работы метода
     * при входном значении списка List<String> ["Moscow", "New York", "London", "Paris", "Berlin", "London", "Berlin"]
     */
    @Test
    void listStream_duplicateSetStream_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<String> trueAnswerArray = Arrays.asList("Berlin", "London");
        assertEquals(trueAnswerArray,hw.duplicateSetStream(stringList));
    }

    /**
     * проверка метода duplicateSetStream на равенство
     * [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * создаем список с [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * и сравниваем его с результоь работы метода при входном значении списка List<User>
     */
    @Test
    void listUser_duplicateSetStream_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<User> trueAnswerArray = Arrays.asList(
                new User("Victor", "Smirnov",  48, 157, 91, "Tomsk")
        );
        assertEquals(trueAnswerArray,hw.duplicateSetStream(userList));
    }

    /**
     * проверка метода duplicateMap на список с элементами [1, 5, 6]
     * создаем список с элементвами [1, 5, 6] и сравниваем его с результоь работы метода
     * при входном значении списка List<Integer> [2, 5, 1, 3, 5, 6, 7, 8, 9, 1, 6]
     */
    @Test
    void listInteger_duplicateMap_test() {
            DuplicateElementsHW910 hw = new DuplicateElementsHW910();
            List<Integer> trueAnswerArray = Arrays.asList(1, 5, 6);
            assertEquals(trueAnswerArray,hw.duplicateMap(integerList));
    }

    /**
     * проверка метода duplicateMap на список с элементами ["Алматы", "Киев"]
     * создаем список с элементвами [Алматы, Киев] и сравниваем его с результатами работы метода
     * при входном значении списка Queue<String> ["Алматы", "Киев", "Минск", "Львов", "Дубна", "Киев", "Ростов", "Алматы"]
     */
    @Test
    void stringQueue_duplicateMap_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<String> trueAnswerArray = Arrays.asList("Алматы", "Киев");
        assertEquals(trueAnswerArray,hw.duplicateMap(stringQueue));
    }

    /**
     * проверка метода duplicateMap на равенство
     * [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * создаем список с [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * и сравниваем его с результоь работы метода при входном значении списка Queue<User>
     */
    @Test
    void userQueue_duplicateMap_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<User> trueAnswerArray = Arrays.asList(
                new User("Victor", "Smirnov",  48, 157, 91, "Tomsk")
        );
        assertEquals(trueAnswerArray,hw.duplicateMap(userQueue));
    }


    /**
     * проверка метода duplicateMapStream на список с элементами [1, 5, 6]
     * создаем список с элементвами [1, 5, 6] и сравниваем его с результоь работы метода
     * при входном значении списка List<Integer> [2, 5, 1, 3, 5, 6, 7, 8, 9, 1, 6]
     */
    @Test
    void listInteger_duplicateMapStream_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<Integer> trueAnswerArray = Arrays.asList(1, 5, 6);
        assertEquals(trueAnswerArray,hw.duplicateMapStream(integerList));
    }


    /**
     * проверка метода duplicateMapStream на список с элементами ["Алматы", "Киев"]
     * создаем список с элементвами [Алматы, Киев] и сравниваем его с результатами работы метода
     * при входном значении списка Queue<String> ["Алматы", "Киев", "Минск", "Львов", "Дубна", "Киев", "Ростов", "Алматы"]
     */
    @Test
    void stringQueue_duplicateMapStream_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<String> trueAnswerArray = Arrays.asList("Алматы", "Киев");
        assertEquals(trueAnswerArray,hw.duplicateMapStream(stringQueue));
    }

    /**
     * проверка метода duplicateMapStream на равенство
     * [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * создаем список с [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * и сравниваем его с результоь работы метода при входном значении списка Queue<User>
     */
    @Test
    void userQueue_duplicateMapStream_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<User> trueAnswerArray = Arrays.asList(
                new User("Victor", "Smirnov",  48, 157, 91, "Tomsk")
        );
        assertEquals(trueAnswerArray,hw.duplicateMapStream(userQueue));
    }


    /**
     * проверка метода duplicateSetManual на список с элементами [1, 5, 6]
     * создаем список с элементвами [1, 5, 6] и сравниваем его с результоь работы метода
     * при входном значении списка List<Integer> [2, 5, 1, 3, 5, 6, 7, 8, 9, 1, 6]
     */
    @Test
    void listInteger_duplicateSetManual_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<Integer> trueAnswerArray = Arrays.asList(1, 5, 6);
        assertEquals(trueAnswerArray,hw.duplicateMapStream(integerList));
    }

    /**
     * проверка метода duplicateSetManual на список с элементами ["Алматы", "Киев"]
     * создаем список с элементвами [Алматы, Киев] и сравниваем его с результатами работы метода
     * при входном значении списка Queue<String> ["Алматы", "Киев", "Минск", "Львов", "Дубна", "Киев", "Ростов", "Алматы"]
     */
    @Test
    void stringQueue_duplicateSetManual_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<String> trueAnswerArray = Arrays.asList("Алматы", "Киев");
        assertEquals(trueAnswerArray,hw.duplicateSetManual(stringQueue));
    }

    /**
     * проверка метода duplicateSetManual на равенство
     * [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * создаем список с [User{ name='Victor' , surname='Smirnov' , age=48', weight=157, height=91, address='Tomsk'}]
     * и сравниваем его с результоь работы метода при входном значении списка Queue<User>
     */
    @Test
    void userQueue_duplicateSetManual_test() {
        DuplicateElementsHW910 hw = new DuplicateElementsHW910();
        List<User> trueAnswerArray = Arrays.asList(
                new User("Victor", "Smirnov",  48, 157, 91, "Tomsk")
        );
        assertEquals(trueAnswerArray,hw.duplicateSetManual(userQueue));
    }

}